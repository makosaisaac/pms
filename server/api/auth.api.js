const route = require('express').Router();

const User = require('../models/users.model');
const { createToken } = require('../authentication/token');
const bcrypt = require('bcryptjs');


route.post('/register', (req, res) => {
    var data = req.body;
    // console.log(data);
    return bcrypt.hash(data.password, 12)
        .then((hashkey) => {
            data.password = hashkey;
            // console.log('Data Is Set',data);
            return new User(data).save()
                .then(savedData => {
                    var token = createToken({
                        _id:savedData.id,
                        employee:savedData.employee,
                        supervisor:savedData.supervisor,
                        hr_officer:savedData.hr_officer,
                        manager:savedData.manager,
                        dvc_vc:savedData.dvc_vc,
                    });
                    var user = {
                        ...savedData._doc,
                        token: token
                    };
                    // console.log(user);
                    return res.send(user);
                })
                .catch(err => {
                    // console.log(err);
                    return res.json(err).status(508);
                });
        })
});

route.post('/login', (req, res) => {
    const data = req.body;
    return User.findOne({ email: data.email }, { _id: 0 })
        .then((DBdata) => {
            // console.log(DBdata);
            if (!!DBdata)
                return bcrypt.compare(data.password, DBdata.password);
            return false;
        })
        .then(confirm => {
            // console.log(confirm);
            if (confirm === true) {
                return User.findOne({ email: data.email });
            }
            return { message: "Invalid username or password try again" };
        })
        .then(result => {
            // console.log(User, result);
            if (!!result.email) {
                var token = createToken({
                    _id:result.id,
                    employee:result.employee,
                    supervisor:result.supervisor,
                    hr_officer:result.hr_officer,
                    manager:result.manager,
                    dvc_vc:result.dvc_vc,
                });
                var user = {
                    ...result._doc,
                    token: token
                };
                return res.send(user);
            }
            return res.send(result);
        })
});

module.exports = route;


