const express = require('express');
const route = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const User = require('../models/users.model');
const ArchiveUser = require('../models/archive.model');
const EmployeeNotis = require('../models/emplyee_notification.model');
const EmployeeRecord = require('../models/employee_record.model');
// const Supervisor = require('../model/supervisor.model');
const moment = require('moment');

route.get('/get-all-employees/:id', (req, res) => {
    const id = req.params.id;
    return User.find({ faculty: id })
        .then((dt) => {
            // console.log(dt);
            return res.send(dt);
        })
        .catch((err) => console.log(err));
});

route.get('/get-user-data/:id', (req, res) => {
    const data = req.params.id;
    return User.findOne({ _id: data }, {
        email: 1, firstname: 1, lastname: 1, _id: 0,
        employee: 1, supervisor: 1, hr_officer: 1, manager: 1, dvc_vc: 1
    }).then(dt => res.send(dt));
});

// get all emplyees assign to a supervisor
route.get('/get-supervisor-employees/:id', (req, res) => {

    // console.log("Employee records ", req.params.id);
    return User.findOne({ _id: req.params.id }, { email: 1, _id: 0 })
        .then(em => {
            // console.log("Email ", em.email);'
            return User.aggregate([

                {
                    $match: { supervisor_name: em.email }
                },

                {
                    $lookup:
                    {
                        from: "employeerecords",
                        localField: "email",
                        foreignField: "email",
                        as: "employee_records"
                    }
                },
                {
                    $group:
                    {
                        _id: "$_id",
                        firstname: { $first: "$firstname" },
                        lastname: { $first: "$lastname" },
                        email: { $first: "$email" },
                        probation: { $first: "$probation" },
                        job_grade: { $first: "$job_grade" },
                        employee_records: { $first: { $last: "$employee_records" } }
                    }
                }
            ]);
            // return User.find({ supervisor_name: em }, { firstname: 1, lastname: 1, email: 1, probation: 1, job_grade: 1 })
        })
        .then(all_em => {
            return res.send(all_em);
        })
        .catch(err => {
            // console.log(err);
            return res.json({ message: err }).status(501)
        });

});

// get all supervisors assign to a manager
route.get('/get-manager-supervisors/:id', async (req, res) => {
    const s_email = await User.findOne({ _id: req.params.id }, { email: 1, _id: 0 });
    var supervisors = await User.find({ manager_name: s_email.email }, { firstname: 1, lastname: 1, email: 1, job_grade: 1 });
    // console.log(s_email);
    // console.log(supervisors);

    // for(var i=0; i<supervisors.length; i++){
    //   let totalemp = await User.aggregate(
    //         [
    //           {
    //             $match: {
    //               supervisor_name: supervisors[i].email
    //             }
    //           },
    //           {
    //             $count: "employees_number"
    //           }
    //         ]
    //       );
    //     console.log("Employees Count", totalemp[0]?.employees_number);
    //     supervisors[i]["number_employees"] = !!totalemp[0]?.employees_number?totalemp[0]?.employees_number:0;
    //     console.log(supervisors[i]);

    // }
    return res.send(supervisors);
});

route.get('/get-employee-record/:id', (req, res) => {
    const data = req.params.id;
    // console.log("ID ",data);
    return User.aggregate([
        { $match: { _id: ObjectId(`${data}`) } },
        {
            $lookup:
            {
                from: 'employeerecords',
                localField: 'email',
                foreignField: 'email',
                as: 'employee_record'
            }
        }
    ])
        .then((result) => {
            // console.log("Result ", result);
            return res.send(result[0]);
        })
        .catch(error => res.send(error));
});

route.get('/get-employees-record-data/:id', async (req, res) => {
    const id = req.params.id;
    const user = await User.findOne({ _id: id }, { email: 1, _id: 0 });
    const data = await User.aggregate([
        { $match: { supervisor_name: user.email } },
        {
            $lookup: {
                from: "employeerecords",
                localField: "email",
                foreignField: "email",
                as: "employees_record"
            }
        }
    ]);

    // console.log("Data ",data);

    return res.send(data);
});

route.post('/create-employee-record', (req, res) => {
    const data = req.body;
    return new EmployeeRecord(data).save().then((dt) => {
        return res.send(dt);
    });
});

route.post('/add-employee-record', (req, res) => {
    const { email, period, ...data } = req.body;
    return EmployeeRecord.updateOne({ email: email, period: period }, {
        $push: data
    }, { upsert: true, multi: true }).then((saved) => {
        // console.log(saved);
        if (saved.modifiedCount > 0)
            return res.send({ message: 'updated successfully' });

        return res.send({ message: 'no changes were made' });
    }).catch(err => { console.log(err); })
});

route.post('/remove-employee-record', (req, res) => {
    const { email, period, ...data } = req.body;
    return EmployeeRecord.updateOne({ email: email, period: period }, {
        $pull: data
    }, { upsert: true, multi: true }).then((saved) => {
        // console.log(saved);
        if (saved.modifiedCount > 0)
            return res.send({ message: 'updated successfully' });

        return res.send({ message: 'no changes were made' });
    }).catch(err => { console.log(err); })
});

route.post('/update-employee-record', (req, res) => {
    const { email, period, ...data } = req.body;
    return EmployeeRecord.updateOne({ email: email, period: period },
        {
            $set: { ...data }
        },
        // {multi:true,upsert:true}
    ).then(update => {
        // console.log(update);
        if (update.modifiedCount > 0)
            return res.send({ "message": "updated" });
        return res.send({ "message": "no update" });
    })
});

route.post('/updateEmployee', (req, res) => {
    const { _id, ...data } = req.body;
    return User.updateOne({ _id },
        {
            $set: data
        },
        {
            upsert: true,
            multi: true
        })
        .then(rt => {
            return res.send(rt);
        })
});

route.post('/bc-grading', (req, res) => {
    const { email, period, ...data } = req.body;
    console.log("Email ", email, "Period ", period);
    return EmployeeRecord.updateOne({
        email: email,
        period: period,
        "bc._id": data.bc
    }, {
        $set: {
            "bc.$.supervisor_rating": data.rating,
            "bc.$.comments": data.comments,
            "bc.$.weight": data.weight
        }
    }).then(dt => {
        return res.send(dt);
    })
});

route.post('/update-bc', (req, res) => {
    const { email, period, ...data } = req.body;
    return EmployeeRecord.updateOne({
        email: email,
        period: period,
        "bc._id": data.bc
    }, {
        $set: {
            "bc.$.behaviour_competencies": data.behaviour_competencies,
            "bc.$.key_indicators": data.key_indicators
        }
    }).then(dt => {
        return res.send(dt);
    })
});

route.post('/kpa-grading', (req, res) => {
    const { email, period, ...data } = req.body;
    return EmployeeRecord.updateOne({
        email: email,
        period: period,
        "kpa._id": data.kpa
    }, {
        $set: {
            "kpa.$.supervisor_rating": data.rating,
            "kpa.$.comments": data.comments,
            "kpa.$.weight": data.weight
        }
    }).then(dt => {
        return res.send(dt);
    })
});

route.post('/update-kpa', (req, res) => {
    const { email, period, ...data } = req.body;
    return EmployeeRecord.updateOne({
        email: email,
        period: period,
        "kpa._id": data.kpa
    }, {
        $set: {
            "kpa.$.key_performance_areas": data.key_performance_areas,
            "kpa.$.key_indicators": data.key_indicators
        }
    }).then(dt => {
        return res.send(dt);
    })
});

route.post('/self-grading', (req, res) => {
    const data = req.body;
    if (!!data.bc) {
        return EmployeeRecord.updateOne({
            email: data.email,
            period: data.period,
            "bc._id": data.bc
        }, {
            $set: {
                "bc.$.self_rating": data.rating
            }
        })
            .then(dt => {
                return res.send(dt);
            });
    } else if (!!data.kpa) {
        return EmployeeRecord.updateOne({
            email: data.email,
            period: data.period,
            "kpa._id": data.kpa
        }, {
            $set: {
                "kpa.$.self_rating": data.rating
            }
        })
            .then(dt => {
                return res.send(dt);
            });
    } else if (!!data.pdp) {
        // console.log(data.pdp);
        return EmployeeRecord.updateOne({
            email: data.email,
            period: data.period,
            "pdp._id": data.pdp
        }, {
            $set: {
                "pdp.$.supervisor_comments": data.supervisor_comments
            }
        })
            .then(dt => {
                return res.send(dt);
            });
    }
});

route.get('/get-all-supervisor/:id', (req, res) => {
    return User.find({ supervisor: true, faculty: req.params.id }, { email: 1, firstname: 1, lastname: 1 })
        .then((data) => res.send(data));
});

route.get('/get-all-manager/:id', (req, res) => {
    return User.find({ manager: true, faculty: req.params.id }, { email: 1, firstname: 1, lastname: 1 })
        .then((data) => res.send(data));
});

route.post('/book-meeting', (req, res) => {
    const data = req.body;
    return EmployeeNotis.findOne({ email: data.email })
        .then(value => {
            if (!!value) {
                return EmployeeNotis.updateOne({ email: data.email },
                    {
                        $push: {
                            meetings:
                            {
                                title: data.title,
                                description: data.description,
                                date_time: data.date_time,
                                sub_date: new Date()
                            }
                        }
                    });
            }
            return new EmployeeNotis({
                email: data.email, meetings: [
                    {
                        title: data.title,
                        description: data.description,
                        date_time: data.date_time,
                        sub_date: new Date()
                    }
                ]
            }).save();
        })
        .then(final => {
            // console.log(final);
            return res.send(final);
        })
        .catch(err => { return res.json({ err: err }).status(501) });
});

// get employees meetings
route.get('/get-meetings/:id', (req, res) => {
    const email = req.params.id;
    return EmployeeNotis.findOne({ email }, { meetings: 1, _id: 0 })
        .then(meetings => {
            const mt = meetings.meetings.sort((a, b) => b.sub_date - a.sub_date);
            res.send(mt);
        })
        .catch(err => res.send([]))
});
// updating meetings
route.post('/update-notis', (req, res) => {
    const data = req.body;
    return EmployeeNotis.updateOne({ email: data.email, "meetings.title": data.title },
        {
            $set: {
                "meetings.$.viewed": true
            }
        })
        .then(notis => {
            return res.send(notis);
        })
        .catch(err => res.send(err));
});

route.get('/get-all-statistics', async (req, res) => {
    const users = await User.find({});
    var department = [];
    var fci = 0, s1 = 0;
    var fen = 0, s2 = 0;
    var fha = 0, s3 = 0;
    var fhs = 0, s4 = 0;
    var fnrs = 0, s5 = 0;
    var fms = 0, s6 = 0;
    var dhr = 0, s7 = 0;
    var tlu = 0, s8 = 0;
    var dir = 0, s9 = 0;
    var df = 0, s10 = 0;
    var dict = 0, s11 = 0;

    users.forEach((el) => {
        if (el.faculty === "Faculty of Computing and Informatics") {
            fci++;
            if (el.supervisor) {
                s1++;
            }
        } else if (el.faculty === "Faculty of Engineering") {
            fen++;
            if (el.supervisor) {
                s2++;
            }
        } else if (el.faculty === "Faculty of Health and Applied Science") {
            fha++;
            if (el.supervisor) {
                s3++;
            }
        } else if (el.faculty === "Faculty of Human Sciences") {
            fhs++;
            if (el.supervisor) {
                s4++;
            }
        } else if (el.faculty === "Faculty of Management Sciences") {
            fms++;
            if (el.supervisor) {
                s5++;
            }
        } else if (el.faculty === "Faculty of Natural Resources and Spatial Sciences") {
            fnrs++;
            if (el.supervisor) {
                s6++;
            }
        } else if (el.faculty === "Department of Human Resource") {
            dhr++;
            if (el.supervisor) {
                s7++;
            }
        } else if (el.faculty === "Department of Teaching and Learning Unit") {
            tlu++;
            if (el.supervisor) {
                s8++;
            }
        } else if (el.faculty === "Department of International Relations") {
            dir++;
            if (el.supervisor) {
                s9++;
            }
        } else if (el.faculty === "Department of Finance") {
            df++;
            if (el.supervisor) {
                s10++;
            }
        } else if (el.faculty === "Department of Information Technology") {
            dict++;
            if (el.supervisor) {
                s11++;
            }
        }
    });

    // const allStats = await User.aggregate([{
    //     $group: {
    //         _id: "$faculty",
    //         employees: {
    //             $sum: {
    //                 $cond: [{ $eq: ["$employee", true] }, 1, 0]
    //             }
    //         },
    //         supervisors: {
    //             $sum: {
    //                 $cond: [{ $eq: ["$supervisor", true] }, 1, 0]
    //             }
    //         },
    //         managers: {
    //             $sum: {
    //                 $cond: [{ $eq: ["$manager", true] }, 1, 0]
    //             }
    //         },
    //         dvcs: {
    //             $sum: {
    //                 $cond: [{ $eq: ["$dvc_vc", true] }, 1, 0]
    //             }
    //         },
    //         hr_officers: {
    //             $sum: {
    //                 $cond: [{ $eq: ["$hr_officer", true] }, 1, 0]
    //             }
    //         },
    //     }
    // }]);

    faculty = [
        { faculty: "Faculty of Computing and Informatics", abb: "FCI", number_of_employees: fci, supervisors: s1 },
        { faculty: "Faculty of Engineering", abb: "FEN", number_of_employees: fen, supervisors: s2 },
        { faculty: "Faculty of Health and Applied Science", abb: "FHAS", number_of_employees: fha, supervisors: s3 },
        { faculty: "Faculty of Human Sciences", abb: "FHS", number_of_employees: fhs, supervisors: s4 },
        { faculty: "Faculty of Natural Resources and Spatial Sciences", abb: "FNRSS", number_of_employees: fnrs, supervisors: s5 },
        { faculty: "Faculty of Management Sciences", abb: "FMS", number_of_employees: fms, supervisors: s6 }
    ];

    department = [
        { faculty: "Department of Human Resource", abb: "DHR", number_of_employees: dhr, supervisors: s7 },
        { faculty: "Department of Teaching and Learning Unit", abb: "DTLU", number_of_employees: tlu, supervisors: s8 },
        { faculty: "Department of International Relations", abb: "DIR", number_of_employees: dir, supervisors: s9 },
        { faculty: "Department of Finance", abb: "DF", number_of_employees: df, supervisors: s10 },
        { faculty: "Department of Information Technology", abb: "DICT", number_of_employees: dict, supervisors: s11 },
    ]

    const usersStats = await User.aggregate([{
        $group: {
            _id: null,
            employees: {
                $sum: {
                    $cond: [{ $eq: ["$employee", true] }, 1, 0]
                }
            },
            supervisors: {
                $sum: {
                    $cond: [{ $eq: ["$supervisor", true] }, 1, 0]
                }
            },
            managers: {
                $sum: {
                    $cond: [{ $eq: ["$manager", true] }, 1, 0]
                }
            },
            dvcs: {
                $sum: {
                    $cond: [{ $eq: ["$dvc_vc", true] }, 1, 0]
                }
            },
            hr_officers: {
                $sum: {
                    $cond: [{ $eq: ["$hr_officer", true] }, 1, 0]
                }
            },
        }
    }]);

    // console.log("users Stats ", usersStats);
    return res.send({ department, faculty, stats: usersStats[0] });
});

// route.get('/get-all-statistics', async (req, res) => {

// });

route.get('/get-employee-data/:id', async (req, res) => {
    const email = req.params.id;
    const data = await EmployeeRecord.find({ email: email });
    return res.send(data);
});

route.post('/delete-employee-pc', async (req, res) => {
    const data = req.body;
    const deletedPeriod = await EmployeeRecord.deleteOne(data);
    // console.log(deletedPeriod);
    return res.send(deletedPeriod);
});


// Archive Employees
route.post("/add-to-archive", async (req, res) => {
    try {
        const { email } = req.body;
        const user = await User.findOneAndDelete({ email }, { _id: 0 });

        // console.log("User ",user);

        const archiveuser = await new ArchiveUser({ ...user._doc }).save();

        return res.json({ message: "successfully added to archive" })
    } catch (error) {
        // console.log(error);
        return res.json({ message: error }).status(500);
    };

});

route.post("/remove-from-archive", async (req, res) => {
    try {
        const { email } = req.body;
        const Auser = await ArchiveUser.findOneAndDelete({ email }, { _id: 0 });
        const user = await new User({ ...Auser._doc }).save();
        return res.json({ message: "successfully removed from archive" });
    } catch (error) {
        // console.log(error);
        return res.json({ message: error }).status(500);
    };
});

route.get("/get-all-archive-employees/:id", async (req, res) => {
    try {
        const id = req.params.id;
        const users = await ArchiveUser.find({ faculty: id })

        res.json(users);
    } catch (error) {
        console.log("Error ", error.toString());
        res.json({ message: error }).status(500);
    }
});

route.get("/get-all-archive-employees/:id", async (req, res) => {
    try {
        const id = req.params.id;
        const users = await ArchiveUser.find({ faculty: id })

        res.json(users);
    } catch (error) {
        console.log("Error ", error.toString());
        res.json({ message: error }).status(500);
    }
});

// complete Contracting and rieviwing
route.post("/complete-contracting-reviewing", async (req, res, next) => {
    try {
        const { email, period, ...status } = req.body;
        const updateRecord = await EmployeeRecord.updateOne({ email, period }, {
            $set: {
                ...status
            }
        });
        // console.log("Record ", updateRecord);
        res.json({ message: "competed" })
    } catch (error) {
        console.log("Error ", error.toString());
        res.json({ message: error }).status(500);
    }
});

module.exports = route;