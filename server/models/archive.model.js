const {model, Schema} = require('mongoose');


const user = new Schema({
    email:{type:String, required:true, unique:true},
    password:{type:String, required:true},
    firstname:{type:String, required:true},
    lastname:{type:String, required:true},
    faculty:{type:String, required:true},
    job_grade:{type:Number, required:true},
    position:{type:String, required:true},
    probation:{type:String, required:true},
    employee:{type:Boolean, required:true,default:true},
    supervisor:{type:Boolean, required:true, default:false},
    hr_officer:{type:Boolean, required:true, default:false},
    manager:{type:Boolean, default:false},
    dvc_vc:{type:Boolean, default:false},
    nature_of_appointment:{type:String,required:true},
    date_of_appointment:{type:Date},
    period_under_review:{type:String},
    supervisor_name:{type:String},
    manager_name:{type:String},
    dvc_name:{type:String}
});

module.exports = model('archive',user);