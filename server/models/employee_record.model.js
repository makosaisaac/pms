const {model,Schema} = require('mongoose');

const KPA = {
    key_performance_areas:{type: String},
    key_indicators:{type: String},
    self_rating:{type:Number,default:0},
    supervisor_rating:{type:Number,default:0},
    comments:{type:String,default:'no available comment'},
    weight:{type:Number,default:0},
};

const BC = {
    behaviour_competencies:{type:String},
    key_indicators:{type: String},
    self_rating:{type:Number,default:0},
    supervisor_rating:{type:Number,default:0},
    comments:{type:String,default:'no available comment'},
    weight:{type:Number,default:0},
};

const PDP =  {
    performance_gap:{type:String},
    intervation_required:{type: String},
    time_frame:{type: String},
    Indicator_for_DGA:{type: String},
    supervisor_comments:{type: String},
}

//Commentary following Performance Planning and Contracting exercise:
const PPC = {
    supervisor_comments:{type: String},
    employee_comments:{type: String},
    employee_approval: {type:String}
};

// Commentary Following Performance Review Exercise:
const PRE = {
    supervisor_comments:{type: String},
    employee_comments:{type: String},
    employee_approval: {type:Boolean},
    supervisor_approval: {type:Boolean},
    major_accomplishments:[{type: String}],
    // Key areas that require further development
    key_areas_needed:{type: String},
    employee_comments:{type: String},
    manager_comments:{type:String},
    dvc_comments:{type:String}
};

const diary = {
    text:{type: String},
    date:{type: Date}
};

const employeeRecord = new Schema({
    email:{type:String,required:true},
    period:{type:String,required:true},
    performance_contracting_status:{type:String,default:"not-done"},
    performance_review_status:{type:String,default:"not-done"},
    ppc:PPC,
    pre:PRE,
    diary:[diary],
    kpa:[KPA],
    bc:[BC],
    pdp:[PDP]
});

module.exports = model('employeeRecord', employeeRecord);

