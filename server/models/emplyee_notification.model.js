const { model, Schema } = require('mongoose');

const employeenot = new Schema({
    email: { type: String, required: true },
    meetings: [
        {
            title: { type: String },
            description: { type: String },
            date_time: { type: Date },
            sub_date: { type: Date },
            viewed: { type: Boolean, default: false }
        }
    ]
});

module.exports = model('employee_notification', employeenot);