const {model, Schema} = require('mongoose');

const supervisor = new Schema({
    username:{type:String,required:true},
    faculty:{type:String,required:true},
    name:{type:String,required:true},
    position:{type:String,required:true}
});


module.exports = model("supervisor",supervisor);