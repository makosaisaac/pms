const express = require('express');
const app = express();
const mongoose = require('mongoose');
const path = require('path');
const dburl = process.env.DB_LINK || "mongodb://localhost:27017/PMS";
const api = require('./api/api');
const auth = require('./api/auth.api');
const { middleware } = require('./authentication/middleware.auth');
// { useNewUrlParser: true }

async function main() {
    const dbconnection = await mongoose.connect(dburl);
    console.log('=============Connections=======================');
    console.log(dbconnection);
    console.log('====================================');
}

main();

app.use(express.json());

if (app.get('env') === 'development') {
    // console.log("Hello world");
    require('dotenv').config();
    const cors = require('cors');
    //app.use(cookieParser())
    app.use(cors())
}

app.use('/authentication', auth);
app.use('/api', middleware, api);

// app.use((request,response,next)=>{
//     if (process.env.NODE_ENV === 'production') {
//         // !request.secure
//         return response.redirect(`https://${request.headers.host}${request.url}`);
//     }
//     next();
// });

//state which directory is for static files
app.use(express.static(path.join(__dirname, '../dist/PerfomanceManagementSystem')));
app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/PerfomanceManagementSystem/index.html'));
});

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Listening to Port ${PORT}`);
});
