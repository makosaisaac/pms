const { verifytoken } = require('../authentication/token');

const middleware = (req, res, next) => {
    try {
        // console.log(req.header('authorization'));
        const token = req.header('authorization').split(' ')[1]
        // console.log(token);
        const check = verifytoken(token);
        // console.log(check);
        if(check._id){
            next();
        }
    } catch (error) {
        console.log(error.toString());
        next(error);
        // res.json({"error":error.toString()}).status(401);
    }
}


module.exports = {
    middleware
}