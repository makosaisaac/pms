const jwt = require('jsonwebtoken');
require('dotenv').config();

function createToken(user) {
  const key = process.env.TOKEN_KEY;
  const token = jwt.sign(user, key, { expiresIn: '1d' });
  return token;
}

function verifytoken(token) {
    let decode = jwt.verify(token, process.env.TOKEN_KEY);
    return decode;
}

module.exports = { createToken, verifytoken }