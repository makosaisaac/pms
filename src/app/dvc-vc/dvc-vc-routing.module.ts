import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DvcVcComponent } from './dvc-vc.component';
import { ViewAllEmployeesComponent } from './view-all-employees/view-all-employees.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';

const routes: Routes = [
  {
    path: "", component: DvcVcComponent,
    children: [
      { path: "view-all-employees/:id", component: ViewAllEmployeesComponent },
      { path: "view-employee/:id", component: ViewEmployeeComponent },
      { path: "dashboard", component: DashboardComponent },
      { path: '', redirectTo: 'dashboard' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DvcVcRoutingModule { }
