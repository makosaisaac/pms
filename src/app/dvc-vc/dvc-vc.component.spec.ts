import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DvcVcComponent } from './dvc-vc.component';

describe('DvcVcComponent', () => {
  let component: DvcVcComponent;
  let fixture: ComponentFixture<DvcVcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DvcVcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DvcVcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
