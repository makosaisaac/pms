import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CalculateService } from 'src/app/services/calculate.service';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.scss']
})
export class ViewEmployeeComponent implements OnInit,OnDestroy {
  pdpdisplayedColumns: string[] = ['performance_gap', 'intervation_required', 'time_frame', 'Indicator_for_DGA','supervisor_comments'];
  bcdisplayedColumns: string[] = ['behaviour_competencies','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  kpadisplayedColumns: string[] = ['key_performance_areas','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  totals:any = 0;
  data:any;
  emp_id:any;
  user:any;
  subs = new SubSink();

  constructor(public calc:CalculateService, private http:ConnectApiService,private act:ActivatedRoute) { }

  comments(form:NgForm){
    const dt = {
      email:this.data?.email,
      period:this.data?.period,
      "pre.dvc_comments":form.value.des
    }
    this.subs.sink = this.http.post('update-employee-record',dt).subscribe((data:any)=>{
      // console.log(data);
      this.getData();    
    });
  }

  getData(){
    this.subs.sink = this.http.get(`get-user-data/${this.emp_id}`).subscribe((data:any)=>{
      this.user = data;
    });
    this.subs.sink = this.http.get(`get-employee-record/${this.emp_id}`).subscribe((data: any) => {
      this.data = data?.employee_record.slice(-1)[0];
      this.totals = this.calc.calculateFinalScore(this.data);
    });
  }
  
  ngOnInit(): void {
    this.emp_id = this.act.snapshot.params.id;
    this.getData();
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

}
