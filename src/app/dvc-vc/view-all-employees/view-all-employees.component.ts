import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';

export interface Employee{
  firstname:string;
  lastname:string;
  position:string;
  supervisor:boolean;
  probation:boolean;
  job_grade:number;
}

@Component({
  selector: 'app-view-all-employees',
  templateUrl: './view-all-employees.component.html',
  styleUrls: ['./view-all-employees.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewAllEmployeesComponent implements OnInit,OnDestroy {
  id:any;
  displayedColumns: string[] = ['firstname', 'lastname', 'position', 'supervisor','probation','job_grade'];
  employees:any[] = [];
  employee:MatTableDataSource<Employee> | any | undefined;
  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort) sort: MatSort | any;
  filterText:string | any;
  subs = new SubSink();

  constructor(private actroute: ActivatedRoute,private http:ConnectApiService,private cd: ChangeDetectorRef) { }

  applyFilter():void{
    this.employee.filter = this.filterText.trim().toLowerCase();
  }

  trackBy(index: number, employee: Object | any) {
    return employee.email;
  }

  ngOnInit(): void {
    this.subs.sink = this.actroute.params.subscribe(route=>{
      // console.log("ACTIVATED ",route.id);
      this.id = route.id;
      
     this.subs.sink =  this.http.get(`get-all-employees/${this.id}`).subscribe((data:any)=>{
        // console.log(data);
        this.cd.detectChanges();
        this.employees = data;
        this.employee = new MatTableDataSource(this.employees);
        // this.employee = this.employees.slice(0,10);
        this.employee.paginator = this.paginator;
        this.employee.sort = this.sort;
      });
    });
  }
  
  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
