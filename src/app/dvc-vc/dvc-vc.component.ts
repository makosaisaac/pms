import { Component, NgZone, OnInit } from '@angular/core';
import { AuthService } from '../authentication/auth.service';
import { ConnectApiService } from '../services/connect-api.service';

export interface FACULTY{
  name:string;
  icon:string;
  abb:string;
}
@Component({
  selector: 'app-dvc-vc',
  templateUrl: './dvc-vc.component.html',
  styleUrls: ['./dvc-vc.component.scss']
})
export class DvcVcComponent implements OnInit {
  faculty:FACULTY[] = [
    {icon:"desktop_windows",name:"Faculty of Computing and Informatics", abb:"FCI"},
    {icon:"engineering",name:"Faculty of Engineering", abb:"FCI"},
    {icon:"health_and_safety",name:"Faculty of Health and Applied Science", abb:"FHAS"},
    {icon:"badge",name:"Faculty of Human Sciences", abb:"FHS"},
    {icon:"park",name:"Faculty of Natural Resources and Spatial Sciences",abb:"FNRSS"},
    {icon:"business_center",name:"Faculty of Management Sciences",abb:"FMS"}
  ];

  department:FACULTY[]=[
    {icon:"emoji_people",name:"Department of Human Resource",abb:"DHR"},
    {icon:"cast_for_education",name:"Department of Teaching and Learning Unit",abb:"TLU"},
    {icon:"public",name:"Department of International Relations",abb:"DIR"},
    {icon:"monetization_on",name:"Department of Finance",abb:"DF"},
    {icon:"sync_alt",name:"Department of Information Technology",abb:"DICT"},
  ];
  user:any;

  constructor(private http:ConnectApiService,private auth:AuthService,private ngzone:NgZone) { }

  switchaccount(){
    var cou = document.querySelector('.carousel');
    cou?.classList.toggle('active');
  }
  ngOnInit(): void {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});

    var id: any = this.auth.getUser(<string>localStorage.getItem("qaZDpEWx4zgDLqL"))?._id;
    this.http.get(`get-user-data/${id}`).subscribe((data:any)=>{
      this.user = data;
      if(this.user.manager===false){
        var p = document.getElementById("el1");
        p?.remove();
      }

      if(this.user.hr_officer===false){
        var p = document.getElementById("el2");
        p?.remove();
      }

      if(this.user.supervisor===false){
        var q = document.getElementById("el3");
        q?.remove();
      }
      
      if(this.user.dvc_vc===true){  
        this.ngzone.runOutsideAngular(()=>{
          var elems = document.querySelectorAll('.carousel');
          var instances = M.Carousel.init(elems, {});
        });
      }
    });
  }

}
