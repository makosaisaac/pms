import { TestBed } from '@angular/core/testing';

import { DvcGuard } from './dvc.guard';

describe('DvcGuard', () => {
  let guard: DvcGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(DvcGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
