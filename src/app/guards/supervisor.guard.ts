import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SupervisorGuard implements CanActivate, CanLoad {

  constructor(private auth:AuthService,private route:Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return  this.checkUser();
  }

  checkUser():boolean{
    const token = localStorage.getItem("qaZDpEWx4zgDLqL");
    const user = this.auth.getUser(<string>token);
    if(user?.supervisor){
      return true
    }
    this.route.navigate(['/auth/login'])
    return false;
  }
}
