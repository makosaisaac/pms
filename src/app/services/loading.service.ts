import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  count: number = 0;
  loading$ = new BehaviorSubject<string>('');
  constructor() { }

  getLoading(): Observable<string> {
    return this.loading$.asObservable();
  }

  requestStarted(): void {
    if (++this.count == 1) {
      this.loading$.next('start');
    }
  }

  requestEnded(): void {
    if (this.count == 0 || --this.count == 0) {
      this.loading$.next('stop');
    }
  }

  resetRequest(): void {
    this.count = 0;
    this.loading$.next('stop');
  }

}
