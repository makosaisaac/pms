import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculateService {

  constructor() { }

  calculateScore(obj:any,perc:number): number{
    var value = perc*(((obj?.supervisor_rating??0)/5)*obj?.weight??0);
    // console.log('Calculates Value', value);
    if(!value){
      value = 0;
    }
    return value;
  }

  calculateFinalScore(obj:any){
    // (rating/overal rating * Weight)* percentage.
    // console.log(obj)
    var totalscore = 0, bc = 0, kpa = 0;
    obj?.bc.forEach((element:any)=>{
      bc += this.calculateScore(element,0.30);
    });

    obj?.kpa.forEach((element:any) =>{
      kpa += this.calculateScore(element,0.70);
    });
    totalscore = bc+kpa;
    return { bc , kpa, totalscore};
  }

  showComment(mark:number):string{
    var comment = "";
    if(mark>=88){
      comment = "Outstanding";
    }else if(mark>=60){
      comment = "Exceeds Expectations";    
    }else if(mark>=55){
      comment = "Meets Expectations";     
    }else if(mark>=50){
      comment = "Requires Further Development";      
    }else if(mark>0){
      comment = "Performance is Unacceptable";
    }else{
      comment = "Competency Not Assessed";
    }

    return comment;
  }
}
