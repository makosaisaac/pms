import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CacheBucket, HttpCacheManager, requestDataChanged, withCache } from '@ngneat/cashew';
import * as moment from "moment";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface FACULTY {
  name: string;
  icon: string;
  abb: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConnectApiService {
  url: string = environment.production ? window.location.origin + '/api/' : "http://localhost:9090/api/";

  faculty: FACULTY[] = [
    { icon: "desktop_windows", name: "Faculty of Computing and Informatics", abb: "FCI" },
    { icon: "engineering", name: "Faculty of Engineering", abb: "FCI" },
    { icon: "health_and_safety", name: "Faculty of Health and Applied Science", abb: "FHAS" },
    { icon: "badge", name: "Faculty of Human Sciences", abb: "FHS" },
    { icon: "park", name: "Faculty of Natural Resources and Spatial Sciences", abb: "FNRSS" },
    { icon: "business_center", name: "Faculty of Management Sciences", abb: "FMS" }
  ];

  department: FACULTY[] = [
    { icon: "emoji_people", name: "Department of Human Resource", abb: "DHR" },
    { icon: "cast_for_education", name: "Department of Teaching and Learning Unit", abb: "TLU" },
    { icon: "public", name: "Department of International Relations", abb: "DIR" },
    { icon: "monetization_on", name: "Department of Finance", abb: "DF" },
    { icon: "sync_alt", name: "Department of Information Technology", abb: "DICT" },
  ];

  appBucket = new CacheBucket();

  constructor(private http: HttpClient, private _snackBar: MatSnackBar, private manager: HttpCacheManager) { }

  time(time: any) {
    return moment(time).format('lll');
  }

  get(data: any) : Observable<any> | any {
    return this.http.get(`${this.url}${data}`, {
      context: withCache({
        bucket: this.appBucket,
        clearCachePredicate: requestDataChanged
      })
    });
  }
  post(link: any, data: any) {
    this.manager.clear();
    return this.http.post(`${this.url}${link}`, data);
  }

  public openSnackBar(message: string): void {
    this._snackBar.open(message, 'undo', {
      duration: 3000,
      panelClass: 'custon-snackbar'
    });
  }

}
