import { NgModule } from '@angular/core';
import { ExtraOptions, PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DvcGuard } from './guards/dvc.guard';
import { EmployeeGuard } from './guards/employee.guard';
import { HrGuard } from './guards/hr.guard';
import { ManagerGuard } from './guards/manager.guard';
import { SupervisorGuard } from './guards/supervisor.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'auth', loadChildren: () => import('../app/authentication/authentication.module').then(module => module.AuthenticationModule) },
  { path: 'employee', canLoad: [EmployeeGuard], loadChildren: () => import('../app/employee/employee.module').then(module => module.EmployeeModule) },
  { path: 'supervisor', canLoad: [SupervisorGuard], loadChildren: () => import('../app/supervisor/supervisor.module').then(module => module.SupervisorModule) },
  { path: 'hr-department', canLoad: [HrGuard], loadChildren: () => import('../app/hr-department/hr-department.module').then(module => module.HrDepartmentModule) },
  { path: 'manager', canLoad: [ManagerGuard], loadChildren: () => import('../app/manager/manager.module').then(module => module.ManagerModule) },
  { path: 'dvc-vc', canLoad: [DvcGuard], loadChildren: () => import('../app/dvc-vc/dvc-vc.module').then(module => module.DvcVcModule) },
  { path: 'instructions', loadChildren: () => import('../app/instruction/instruction.module').then(module => module.InstructionModule) },
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: "enabled",
  anchorScrolling: "enabled",
  preloadingStrategy: PreloadAllModules
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
