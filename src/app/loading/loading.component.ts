import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  subs = new SubSink();
  loadingbar: boolean = false;
  constructor(private LoadingService: LoadingService, private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.start();
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

  start(): void {
    this.subs.sink = this.LoadingService.getLoading().subscribe((status: string) =>this.loadingbar = status === "start");
    this.cd.detectChanges();
  }

}
