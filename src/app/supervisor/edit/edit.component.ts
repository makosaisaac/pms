import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {
  editForm: UntypedFormGroup | any;
  subs = new SubSink();
  constructor(public dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http: ConnectApiService, private _formBuilder: UntypedFormBuilder) { }

  save(): void {
    if (this.data?.type === 'bc') {
      this.submitBC();
    } else {
      this.submitKPA();
    }
  }

  delete(): void {
    if (this.data?.type === 'bc') {
      this.deleteBC();
    } else {
      this.deleteKPA();
    }
  }

  submitBC(): void {
    let bc = {
      bc: this.data._id,
      behaviour_competencies: this.editForm.value.key,
      key_indicators: this.editForm.value.description
    }
    let data = !!this.data?.behaviour_competencies ? {
      email: this.data.email,
      period: this.data?.period,
      ...bc
    } : {
      email: this.data.email,
      period: this.data?.period,
      bc
    };

    this.subs.sink = this.http.post(`${!!this.data?.behaviour_competencies ? 'update-bc' : 'add-employee-record'}`, data).subscribe((data: any) => {
      // console.log(data);
      if (data?.modifiedCount > 0 || data?.message === 'updated successfully') {
        this.editForm.reset();
        this.http.openSnackBar("updated successfully 👍");
        this.dialogRef.close('ok');
      } else {
        this.http.openSnackBar("no changes");
      }

    });
  }

  deleteBC(): void {
    const bc = {
      behaviour_competencies: this.editForm.value.key,
      key_indicators: this.editForm.value.description
    }
    this.subs.sink = this.http.post('remove-employee-record', {
      email: this.data.email,
      period: this.data?.period,
      bc
    }).subscribe((data: any) => {
      if (data?.modifiedCount > 0 || data?.message === 'updated successfully') {
        this.editForm.reset();
        this.http.openSnackBar("deleted successfully 👍");
        this.dialogRef.close('ok');
      } else {
        this.http.openSnackBar("no changes");
      }
    })
  }

  submitKPA(): void {
    const kpa = {
      kpa: this.data._id,
      key_performance_areas: this.editForm.value.key,
      key_indicators: this.editForm.value.description
    }

    let data = !!this.data?.key_performance_areas ? {
      email: this.data.email,
      period: this.data?.period,
      ...kpa
    } : {
      email: this.data.email,
      period: this.data?.period,
      kpa
    };
    this.subs.sink = this.subs.sink = this.http.post(`${!!this.data?.key_performance_areas ? 'update-kpa' : 'add-employee-record'}`, data).subscribe((data: any) => {
      console.log(data);
      if (data?.modifiedCount > 0 || data?.message === 'updated successfully') {
        this.editForm.reset();
        this.http.openSnackBar("updated successfully 👍");
        this.dialogRef.close('ok');
      } else {
        this.http.openSnackBar("no changes");
      }
    })
  }

  deleteKPA(): void {
    const kpa = {
      key_performance_areas: this.editForm.value.key,
      key_indicators: this.editForm.value.description
    }
    this.subs.sink = this.subs.sink = this.http.post('remove-employee-record', {
      email: this.data.email,
      period: this.data?.period,
      kpa
    }).subscribe((data: any) => {
      if (data?.modifiedCount > 0 || data?.message === 'updated successfully') {
        this.editForm.reset();
        this.http.openSnackBar("deleted successfully 👍");
        this.dialogRef.close('ok');
      } else {
        this.http.openSnackBar("no changes");
      }
    })
  }

  ngOnInit(): void {
    this.editForm = this._formBuilder.group({
      key: [this.data?.behaviour_competencies || this.data?.key_performance_areas, Validators.required],
      description: [this.data?.key_indicators, Validators.required]
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
