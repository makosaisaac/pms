import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ConnectApiService } from '../services/connect-api.service';
import * as M from 'materialize-css';
import { SubSink } from 'subsink';
import { AuthService } from '../authentication/auth.service';

@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.scss']
})
export class SupervisorComponent implements OnInit, OnDestroy {
  user: any;
  subs = new SubSink();
  // accounts = ["Supervisor", "Employee"];
  constructor(private http: ConnectApiService,private auth:AuthService,private ngzone:NgZone) { }

  switchaccount() {
    var cou = document.querySelector('.carousel');
    cou?.classList.toggle('active');
  }

  ngOnInit(): void {
    var id: any = this.auth.getUser(<string>localStorage.getItem("qaZDpEWx4zgDLqL"))?._id;
    this. subs.sink = this.http.get(`get-user-data/${id}`).subscribe((data: any) => {
      this.user = data;

      if (this.user.manager === false) {
        var p = document.getElementById("el1");
        p?.remove();
      }

      if (this.user.dvc_vc === false) {
        var p = document.getElementById("el2");
        p?.remove();
      }

      if (this.user.hr_officer === false) {
        var q = document.getElementById("el3");
        q?.remove();
      }

      if (this.user.supervisor === true) {
        this.ngzone.runOutsideAngular(()=>{
          var elems = document.querySelectorAll('.carousel');
          var instances = M.Carousel.init(elems, {});
        });
      }

    });

  }
  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
