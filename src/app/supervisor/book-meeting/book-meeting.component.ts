import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-book-meeting',
  templateUrl: './book-meeting.component.html',
  styleUrls: ['./book-meeting.component.scss']
})
export class BookMeetingComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<BookMeetingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http:ConnectApiService) { }

  
    bookMeeting(form:NgForm){
      var book = {
        email:this.data.email,
        title:form.value.title,
        description:form.value.des,
        date_time:form.value.date
      }

      this.http.post('book-meeting',book).subscribe(data=>{
        console.log(data);
        this.dialogRef.close();
      })
    }

  ngOnInit(): void {
  }

}
