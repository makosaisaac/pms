import { Component, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CalculateService } from 'src/app/services/calculate.service';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import * as moment from "moment";
import { MatDialog } from '@angular/material/dialog';
import { GradingComponent } from '../grading/grading.component';
import { CreateRecordComponent } from '../create-record/create-record.component';
import { PdpCommentComponent } from '../pdpcomment/pdpcomment.component';
import { BookMeetingComponent } from '../book-meeting/book-meeting.component';
import { CalenderComponent } from '../calender/calender.component';
import { SubSink } from 'subsink';
import { EditComponent } from '../edit/edit.component';
// import * as M from "materialize-css";

@Component({
  selector: 'app-view-employee-deatails',
  templateUrl: './view-employee-details.component.html',
  styleUrls: ['./view-employee-details.component.scss', './view-employee-details1.component.scss']
})
export class ViewEmployeeDetailsComponent implements OnInit, OnDestroy {
  em_id: any;
  em_data: any;
  hide: boolean = false;
  recordPeriod: any;
  currentPeriod: any;
  instances: any;
  subs = new SubSink();

  ppc: UntypedFormGroup | any;
  pre: UntypedFormGroup | any;

  constructor(public calc: CalculateService, private http: ConnectApiService, private actroute: ActivatedRoute, private dialog: MatDialog, private _formBuilder: UntypedFormBuilder) { }
  // bc
  bcdisplayedColumns: string[] = ['behaviour_competencies', 'key_indicators', 'self_rating', 'supervisor_rating', 'comments', 'weight', 'final_score'];
  bcdataSource = [];
  // kpa
  kpadisplayedColumns: string[] = ['key_performance_areas', 'key_indicators', 'self_rating', 'supervisor_rating', 'comments', 'weight', 'final_score'];
  kpadataSource = [];
  // pdp
  pdpdisplayedColumns: string[] = ['performance_gap', 'intervation_required', 'time_frame', 'Indicator_for_DGA', 'supervisor_comments'];
  pdpdataSource = [];

  BtrackBy(index: number, item: any) {
    return item.behaviour_competencies;
  }
  KtrackBy(index: number, item: any) {
    return item.key_performance_areas;
  }
  PtrackBy(index: number, item: any) {
    return item.performance_gap;
  }
  NtrackBy(index: number, item: any) {
    return item.date;
  }

  hideperf: boolean = false;
  hidePerformance() {
    this.hideperf = !this.hideperf;
  }

  // rate an Employee 
  updateEmployee(data: any, type: string) {
    if (this.currentPeriod?.performance_contracting_status === "not-done") {
      this.add_BC_KPA(data, type);
      return
    }

    if (this.currentPeriod?.performance_review_status === "not-done") {
      const dialog = this.dialog.open(GradingComponent, {
        width: '30%',
        minWidth: '350px',
        data: { ...data, email: this.currentPeriod.email, period: this.currentPeriod.period }
      });

      this.subs.sink = dialog.afterClosed().subscribe((dt: any) => {
        // console.log(dt);
        if (dt?.updated) {
          this.getData();
        }
      })
    }
  }

  add_BC_KPA = (data: any, type: string): void => {
    let editDialog = this.dialog.open(EditComponent, {
      data: {
        type,
        ...data,
        email: this.currentPeriod.email,
        period: this.currentPeriod.period
      },
      width: "30%"
    });

    this.subs.sink = editDialog.afterClosed().subscribe((dt: any) => {
      // console.log(dt);
      if (dt === 'ok') {
        this.getData();
      }
    });
  }

  // submitBC(form: NgForm) {
  //   const bc = {
  //     behaviour_competencies: form.value.bca,
  //     key_indicators: form.value.kpi
  //   }
  //   this.subs.sink = this.http.post('add-employee-record', {
  //     email: this.em_data.email,
  //     period: this.currentPeriod?.period,
  //     bc
  //   }).subscribe((data: any) => {
  //     // console.log(data);
  //     form.reset();
  //     this.getData();
  //   })
  // }

  // submitKPA(form: NgForm) {
  //   const kpa = {
  //     key_performance_areas: form.value.kpa,
  //     key_indicators: form.value.kpi
  //   }
  //   this.subs.sink = this.subs.sink = this.http.post('add-employee-record', {
  //     email: this.em_data.email,
  //     period: this.currentPeriod?.period,
  //     kpa
  //   }).subscribe((data: any) => {
  //     form.reset();
  //     // console.log(data);
  //     this.getData();
  //   })
  // }

  // completing contracting
  completeContracting = () => {
    this.subs.sink = this.http.post('complete-contracting-reviewing', {
      email: this.em_data.email,
      period: this.currentPeriod?.period,
      performance_contracting_status: 'done'
    }).subscribe((data: any) => {
      this.http.openSnackBar(data?.message);
      if (data?.message)
        this.getData();
    })
  }

  // completing performance review
  completeReviewing = () => {
    this.subs.sink = this.http.post('complete-contracting-reviewing', {
      email: this.em_data.email,
      period: this.currentPeriod?.period,
      performance_review_status: 'done'
    }).subscribe((data: any) => {
      const co = document.querySelector('.confirm-outer');
      co?.classList.toggle('active');
      this.create = !this.create
      this.http.openSnackBar(data?.message);
      if (data?.message)
        this.getData();
    })
  }

  assignCurrentPeriod(period: any) {
    this.currentPeriod = period;
    this.bcdataSource = this.currentPeriod?.bc;
    this.kpadataSource = this.currentPeriod?.kpa;
    this.pdpdataSource = this.currentPeriod?.pdp;
    this.totalscore = this.calc.calculateFinalScore(this.currentPeriod).totalscore.toFixed(0);
  }

  addRecord() {
    const addrecord = this.dialog.open(CreateRecordComponent, {
      width: '40%',
      data: !!this.currentPeriod ? this.currentPeriod : { email: this.em_data?.email }
    });
    this.subs.sink = addrecord.afterClosed().subscribe((dt: any) => {
      // console.log(dt);
      if (!!dt)
        this.getData();
    });
  }

  openCalender() {
    this.subs.sink = this.http.get(`get-meetings/${this.em_data?.email}`).subscribe((data: any) => {
      var data = data.map((mp: any) => ({ title: mp.title, date: mp.date_time }));
      const calendar = this.dialog.open(CalenderComponent, {
        width: '60%',
        height: '80vh',
        data: data
      });
    });
  }


  // saveButton(value: any): void {
  //   console.log(value);
  //   const dt = {
  //     email: "",
  //     period: "",
  //   };
  // }

  // delete performance cycle
  deletePC(period: string): void {
    this.subs.sink = this.http.post("delete-employee-pc", {
      email: this.em_data.email,
      period: period
    }).subscribe((data: any) => {
      // console.log(data);
      if (data?.deletedCount === 1)
        this.getData();
    })
  }

  // Note Panel
  sidepanel: boolean = false;
  hideSidePanel() {
    this.sidepanel = !this.sidepanel;
  }

  notes: string = '';
  uploadNotes() {
    const diary = {
      text: this.notes,
      date: new Date()
    };

    this.subs.sink = this.http.post('add-employee-record', {
      email: this.em_data.email,
      period: this.currentPeriod?.period,
      diary
    }).subscribe((data: any) => {
      this.notes = '';
      // console.log(data);
      this.getData();
    })
  }

  time(time: any) {
    return moment(time).calendar();
  }

  updatePDPComment(data: any) {
    if (!this.currentPeriod?.pre?.supervisor_comments) {
      const dialog = this.dialog.open(PdpCommentComponent, {
        width: '30%',
        minWidth: '350px',
        data: { ...data, email: this.currentPeriod.email, period: this.currentPeriod.period }
      });

      this.subs.sink = dialog.afterClosed().subscribe((dt: any) => {
        // console.log(dt);
        if (dt?.updated) {
          this.getData();
        }
      });
    }
  }

  create: boolean = false;
  activate() {
    this.create = !this.create;
    const co = document.querySelector('.confirm-outer');
    co?.classList.toggle('active');
  }

  majorc: any;
  comment(): void {
    this.majorc = this.instances[0].chipsData.map((da: any) => da.tag);

    if (!this.currentPeriod?.pre?.supervisor_comments) {
      var com = {
        email: this.currentPeriod.email,
        period: this.currentPeriod.period,
        "pre.supervisor_comments": this.pre.value.supervisor_comments,
        "pre.major_accomplishments": this.majorc,
        "pre.key_areas_needed": this.pre.value.key_areas_needed,
        "ppc.supervisor_comments": this.ppc.value.supervisor_comments
      }

      this.subs.sink = this.http.post('update-employee-record', com).subscribe((data: any) => {
        // console.log(data);
        const co = document.querySelector('.confirm-outer');
        co?.classList.toggle('active');
        this.create = !this.create;
        this.http.openSnackBar(data?.message);
        if (data.message === 'updated')
          this.getData();
      });
    }
  }

  bookmeeting() {
    const dialog = this.dialog.open(BookMeetingComponent, {
      data: { email: this.em_data.email },
      width: '30%',
      minWidth: '350px'
    });

    this.subs.sink = dialog.afterClosed().subscribe((close: any) => {
      if (!!close) {
        this.getData();
      }
    })
  }

  totalscore: any;
  getData(): void {
    this.subs.sink = this.http.get(`get-employee-record/${this.em_id}`).subscribe((data: any) => {
      this.currentPeriod = data?.employee_record.slice(-1)[0];
      this.em_data = data;
      this.bcdataSource = this.currentPeriod?.bc;
      this.kpadataSource = this.currentPeriod?.kpa;
      this.pdpdataSource = this.currentPeriod?.pdp;

      this.totalscore = this.calc.calculateFinalScore(this.currentPeriod).totalscore.toFixed(0);

      this.em_id = this.actroute.snapshot.params.id;
      this.ppc = this._formBuilder.group({
        supervisor_comments: [this.currentPeriod?.ppc?.supervisor_comments, Validators.required]
      });

      this.pre = this._formBuilder.group({
        supervisor_comments: [this.currentPeriod?.pre?.supervisor_comments, Validators.required],
        // major_accomplishments: [this.currentPeriod?.pre?.major_accomplishments, Validators.required],
        key_areas_needed: [this.currentPeriod?.pre?.key_areas_needed, Validators.required]
      });

      var elems = document.querySelectorAll('.ch');
      
      if(!!this.currentPeriod){
        this.instances = M.Chips.init(elems, {
          data: this.currentPeriod?.pre?.major_accomplishments?.map((x: any) => { return { tag: x } }),
          placeholder: "press enter to add more"
        });
      }
    })
  }


  ngOnInit(): void {
    this.em_id = this.actroute.snapshot.params.id;
    this.ppc = this._formBuilder.group({
      supervisor_comments: ["", Validators.required]
    });

    this.pre = this._formBuilder.group({
      supervisor_comments: ["", Validators.required],
      // major_accomplishments: ["", Validators.required],
      key_areas_needed: ["", Validators.required]
    });

    var elems = document.querySelectorAll('.ch');
    this.instances = M.Chips.init(elems, {
      placeholder: "press enter to add more"
    });

    this.getData();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
