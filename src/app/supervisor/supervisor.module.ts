import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatRippleModule} from '@angular/material/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin
// import { EditorModule } from '@tinymce/tinymce-angular';

import { SupervisorRoutingModule } from './supervisor-routing.module';
import { SupervisorComponent } from './supervisor.component';
import { ViewAllEmployeesComponent } from './view-all-employees/view-all-employees.component';
import { PdpCommentComponent } from './pdpcomment/pdpcomment.component';
import { ViewEmployeeDetailsComponent } from './view-employee-details/view-employee-details.component';
import { GradingComponent } from './grading/grading.component';
import { BookMeetingComponent } from './book-meeting/book-meeting.component';
import { CreateRecordComponent } from './create-record/create-record.component';
import { CalenderComponent } from './calender/calender.component';
import { EditComponent } from './edit/edit.component';


FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [
    SupervisorComponent,
    ViewAllEmployeesComponent,
    PdpCommentComponent,
    ViewEmployeeDetailsComponent,
    GradingComponent,
    BookMeetingComponent,
    CreateRecordComponent,
    CalenderComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    SupervisorRoutingModule,
    MatTableModule,
    MatIconModule,
    MatRippleModule,
    MatTooltipModule,
    MatMenuModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTabsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FullCalendarModule,
    ReactiveFormsModule,FormsModule
  ]
})
export class SupervisorModule { }
