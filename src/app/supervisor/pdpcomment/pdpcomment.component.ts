import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-pdpcomment',
  templateUrl: './pdpcomment.component.html',
  styleUrls: ['./pdpcomment.component.scss']
})
export class PdpCommentComponent implements OnInit {
  pdpForm:UntypedFormGroup | any;

  constructor(public dialogRef: MatDialogRef<PdpCommentComponent >,
    @Inject(MAT_DIALOG_DATA) public data: any, private http:ConnectApiService,
    private _formBuilder:UntypedFormBuilder) { }

  submitBC(){
    const data = {
      email:this.data.email,
      period:this.data.period
    }

    this.http.post('self-grading',{...data,...this.pdpForm.value,pdp:this.data._id}).subscribe((data:any)=>{
      if(data.modifiedCount>0){
        this.dialogRef.close({updated:true});
      }
    });

  }

  ngOnInit(): void {
    this.pdpForm = this._formBuilder.group({
      supervisor_comments:[this.data?.supervisor_comments, Validators.required]
    });
  }

}
