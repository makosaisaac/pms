import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdpCommentComponent } from './pdpcomment.component';

describe('ViewAllKpaComponent', () => {
  let component: PdpCommentComponent;
  let fixture: ComponentFixture<PdpCommentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdpCommentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdpCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
