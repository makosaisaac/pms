import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-grading',
  templateUrl: './grading.component.html',
  styleUrls: ['./grading.component.scss']
})
export class GradingComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<GradingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http: ConnectApiService,
    private _formBuilder: UntypedFormBuilder) { }

  gradingForm: UntypedFormGroup | any;

  submitBC() {
    const data = {
      email: this.data.email,
      period: this.data.period
    }

    if (!!this.data.behaviour_competencies) {
      this.http.post('bc-grading', { ...data, ...this.gradingForm.value, bc: this.data._id }).subscribe((data: any) => {
        if (data.modifiedCount > 0) {
          this.dialogRef.close({ updated: true })
        }
      });
    } else if (!!this.data.key_performance_areas) {
      this.http.post('kpa-grading', { ...data, ...this.gradingForm.value, kpa: this.data._id }).subscribe((data: any) => {
        if (data.modifiedCount > 0) {
          this.dialogRef.close({ updated: true });
        }
      });
    }
  }

  ngOnInit(): void {
    // console.log(this.data);
    this.gradingForm = this._formBuilder.group({
      rating: [this.data.supervisor_rating, Validators.required],
      weight: [this.data.weight, Validators.required],
      comments: [this.data.comments, Validators.required]
    });

    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
  }

}


