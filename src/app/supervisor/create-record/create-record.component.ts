import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-create-record',
  templateUrl: './create-record.component.html',
  styleUrls: ['./create-record.component.scss']
})
export class CreateRecordComponent implements OnInit {

  createRecordForm: UntypedFormGroup | any;

  range = new UntypedFormGroup({
    start: new UntypedFormControl(),
    end: new UntypedFormControl(),
  });

  constructor(public dialogRef: MatDialogRef<CreateRecordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http: ConnectApiService, private _builder: UntypedFormBuilder) { }


  addRecord() {
    // console.log("Range " ,this.range.value);
    var record;
    if (this.createRecordForm.value.chooseperiod === "yes") {
      var bc = [], kpa = [];
      if (this.data?.bc?.length > 0) {
        bc = this.data?.bc.map((el: any) => {
          return { behaviour_competencies: el.behaviour_competencies, key_indicators: el.key_indicators }
        })
      }

      if (this.data?.kpa?.length > 0) {
        kpa = this.data?.kpa.map((el: any) => {
          return { key_performance_areas: el.key_performance_areas, key_indicators: el.key_indicators }
        });
      }

      record = {
        email: this.data?.email,
        period: `${moment(this.createRecordForm.value.startTime).format('ll')} - ${moment(this.createRecordForm.value.endTime).format('ll')} ${this.createRecordForm.value.recordPeriod}`,
        bc,
        kpa
      };
    } else {
      record = {
        email: this.data?.email,
        period: `${moment(this.createRecordForm.value.startTime).format('ll')} - ${moment(this.createRecordForm.value.endTime).format('ll')} ${this.createRecordForm.value.recordPeriod}`
      }
    }

    this.http.post('create-employee-record', record).subscribe((data: any) => {
      // console.log(data);
      if (!!data)
        this.dialogRef.close("sucessfully created");
    })
  }


  ngOnInit(): void {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
    // console.log(this.data);
    this.createRecordForm = this._builder.group({
      recordPeriod: ['', Validators.required],
      // time:['', Validators.required],
      chooseperiod: ["yes", Validators.required],
      startTime: ["", Validators.required],
      endTime: ["", Validators.required]
    });
  }

}
