import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CalendarOptions } from '@fullcalendar/angular';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss']
})
export class CalenderComponent implements OnInit {
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth'
  };
  constructor(public dialogRef: MatDialogRef<CalenderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http:ConnectApiService) { }

  ngOnInit(): void {
    this.calendarOptions.events = this.data;
  }

}
