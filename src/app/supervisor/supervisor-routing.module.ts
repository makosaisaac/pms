import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupervisorComponent } from './supervisor.component';
import { ViewAllEmployeesComponent } from './view-all-employees/view-all-employees.component';
import { ViewEmployeeDetailsComponent } from './view-employee-details/view-employee-details.component';

const routes: Routes = [
  {
    path:'', component:SupervisorComponent,
    children:[
      {path:'view-all-employees',component:ViewAllEmployeesComponent},
      {path:'view-update-employee-details/:id',component:ViewEmployeeDetailsComponent},
      {path:'',redirectTo:'/supervisor/view-all-employees',pathMatch:'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupervisorRoutingModule { }
