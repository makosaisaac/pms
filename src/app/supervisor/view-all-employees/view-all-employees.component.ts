import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { Chart, registerables } from 'chart.js';
import { SubSink } from 'subsink';
Chart.register(...registerables);

// c-> contracting
// r-> reviewing
export interface Status{
  c_pending:number;
  c_done:number;
  r_pending:number;
  r_done:number;
}

@Component({
  selector: 'app-view-all-employees',
  templateUrl: './view-all-employees.component.html',
  styleUrls: ['./view-all-employees.component.scss']
})
export class ViewAllEmployeesComponent implements OnInit, OnDestroy {
  employees: any[] = [];
  employees1: any[] = [];
  piegraph: any;
  searchfilter: string = "";
  completed: number = 0;
  pending: number = 0;
  perfomance_status:Status | any = {c_done:0,c_pending:0,r_pending:0,r_done:0};
  private subs = new SubSink();

  constructor(private http: ConnectApiService,private ngzone:NgZone) { }

  pieChart(): void {
    var pie = document.getElementById("pie") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(()=>{
      this.piegraph = new Chart(pie, {
        // type: 'doughnut',
        type: 'pie',
        data: {
          labels: [
            'Task Pending',
            'Task Done'
          ],
          datasets: [
          //   {
          //   label: 'Performance Contracting',
          //   data: [this.perfomance_status?.c_pending, this.perfomance_status?.c_done],
          //   backgroundColor: [
          //     '#D22730',
          //     '#f0b323',
          //     // '#002469'
          //   ],
          //   hoverOffset: 4,
          //   borderRadius:1
          // },
          {
            label: 'Performane Reviewing',
            data: [this.perfomance_status?.r_pending, this.perfomance_status?.r_done],
            backgroundColor: [
              '#D22730',
              '#f0b323',
              // '#1b2c5d'
              // '#002469'
            ],
            hoverOffset: 4,
            borderWidth: 1,
            // borderColor:"#1b2c5d"
          }
        ]
        },
        options: {
          responsive: true,
          cutout: '80%',
          animation: { animateRotate: true },
          plugins:{
            tooltip:{
              enabled:true
            },
            title:{
              display:true,
              text:"Performance Review and Accessment"
            }
          }
        }
      });
    });
  }

  trackBy(index: number, employee: Object | any) {
    return employee.email;
  }

  checkTask(employ: any[]): void {
    let cp: number = 0,cd:number=0 ,rp:number =0,rd:number = 0;

    employ.forEach((employee: any) => {
      if (employee.employee_records?.performance_contracting_status==="not-done") {
        // this.completed++;
        cp++;
      } else {
        // this.pending++;
        cd++;
      }

      if (employee.employee_records?.performance_review_status==="not-done") {
        // this.completed++;
        rp++;
      } else {
        // this.pending++;
        rd++;
      }
    });

    this.perfomance_status = {
      c_pending:cp,
      c_done:cd,
      r_pending:rp,
      r_done:rd
    }

    // console.log(this.perfomance_status);
    

    // console.log("Completed ", this.completed);
    // console.log("Pending ", this.pending);
    this.pieChart();
  }

  searchEmployee(): void {
    if (this.searchfilter === "") {
      this.employees = this.employees1;
    } else {
      this.employees = this.employees1.filter((data: any) => {
        if (data?.firstname?.toLowerCase().match(this.searchfilter.toLowerCase()) || data?.lastname?.toLowerCase().match(this.searchfilter.toLowerCase())) {
          return data;
        }
      })
    }
  }

  ngOnInit(): void {
    var id: any = localStorage.getItem('id');
    this.subs.sink = this.http.get(`get-supervisor-employees/${id}`).subscribe((data: any) => {
      this.employees = data;
      this.employees1 = data;
      // console.log(data);
      this.checkTask(data);
      
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
