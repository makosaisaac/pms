import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDepartmentsComponent } from './change-departments.component';

describe('ChangeDepartmentsComponent', () => {
  let component: ChangeDepartmentsComponent;
  let fixture: ComponentFixture<ChangeDepartmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeDepartmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
