import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-change-departments',
  templateUrl: './change-departments.component.html',
  styleUrls: ['./change-departments.component.scss']
})
export class ChangeDepartmentsComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  _id: any;
  constructor(private _bottomSheetRef: MatBottomSheetRef<ChangeDepartmentsComponent>, public http: ConnectApiService,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) { }

  change(event: MouseEvent, value: any): void {
    this.subs.sink = this.http.post('updateEmployee', {
      _id: this._id,
      faculty: value
    }).subscribe((res:any | Object) => {
      if(res?.modifiedCount >0){
        this.http.openSnackBar(`${this.data.name} has moved to ${value}`);
        this._bottomSheetRef.dismiss("closed");
        event.preventDefault();
      }else{
        this._bottomSheetRef.dismiss();
        event.preventDefault();
      }
    });
  }

  ngOnInit(): void {
    this._id = this.data._id
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
