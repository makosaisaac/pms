import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatRippleModule} from '@angular/material/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import {MatBadgeModule} from '@angular/material/badge';
import {NgxPrintModule} from 'ngx-print';
import {MatListModule} from '@angular/material/list';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';

import { HrDepartmentRoutingModule } from './hr-department-routing.module';
import { HrDepartmentComponent } from './hr-department.component';
import { AssignComponent } from './assign/assign.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShowEmployeeRecordComponent } from './show-employee-record/show-employee-record.component';
import { ChangeDepartmentsComponent } from './change-departments/change-departments.component';
import { UpdateUserInfoComponent } from './update-user-info/update-user-info.component';

@NgModule({
  declarations: [
    HrDepartmentComponent,
    AssignComponent,
    ViewEmployeeComponent,
    DashboardComponent,
    ShowEmployeeRecordComponent,
    ChangeDepartmentsComponent,
    UpdateUserInfoComponent
  ],
  imports: [
    CommonModule,
    HrDepartmentRoutingModule,
    MatTableModule,
    MatIconModule,
    MatRippleModule,
    MatTooltipModule,
    MatMenuModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDialogModule,
    MatBadgeModule,
    NgxPrintModule,
    MatListModule,
    MatBottomSheetModule,
    ReactiveFormsModule,FormsModule
  ]
})
export class HrDepartmentModule { }
