import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';
import { ChangeDepartmentsComponent } from '../change-departments/change-departments.component';

export interface Employee {
  firstname: string;
  lastname: string;
  position: string;
  supervisor: boolean;
  probation: boolean;
  job_grade: number;
}
@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewEmployeeComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  id: any;
  displayedColumns: string[] = ['firstname', 'lastname', 'position', 'supervisor', 'probation', 'job_grade','archive'];
  employees: any[] = [];
  archived_employees$ = new Observable<any>();
  employee: MatTableDataSource<Employee> | any | undefined;
  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort) sort: MatSort | any;
  filterText: string | any;

  constructor(private actroute: ActivatedRoute, private http: ConnectApiService, private cd: ChangeDetectorRef, private _bottomSheet: MatBottomSheet) { 
  }

  openBottomSheet(value:any): void {    
    this._bottomSheet.open(ChangeDepartmentsComponent, {
      data: {
        _id: value._id,
        name: `${value.firstname} ${value.lastname}`
      }
    }).afterDismissed().subscribe((close)=>{
      if(close==="closed"){
        this.getData();
      }
    });
  }

  applyFilter() {
    this.employee.filter = this.filterText.trim().toLowerCase();
  }

  add_archive(email:string):void{
    // console.log(email);
    this.subs.sink = this.http.post('add-to-archive',{email}).subscribe((data:any)=>{
      // console.log("Received ",data);
      this.http.openSnackBar(data.message);
      this.getData();
    });
  }
  remove_archive(email:string):void{
    // console.log(email);
    this.subs.sink = this.http.post('remove-from-archive',{email}).subscribe((data:any)=>{
      // console.log("Received ",data);
      this.http.openSnackBar(data.message);
      this.getData();
    });
  }

  show:boolean = false;
  hideArchive():void{
    const arch_employees = document.querySelector(".archive-employees") as HTMLDivElement;
    arch_employees?.classList.toggle("active");
    this.show = !this.show;
  }

  trackBy(index: number, employee: Object | any) {
    return employee.email;
  }

  getData():void{
    this.subs.sink = this.actroute.params.subscribe(route => {
      this.id = route.id;
      this.subs.sink = this.http.get(`get-all-employees/${this.id}`).subscribe((data: any) => {
        // console.log(data);
        this.cd.detectChanges();
        this.employees = new Array(...data);
        this.employee = new MatTableDataSource(this.employees);
        // this.employee = this.employees.slice(0,10);
        this.employee.paginator = this.paginator;
        this.employee.sort = this.sort;
      });

      this.archived_employees$ = this.http.get(`get-all-archive-employees/${this.id}`);
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}

