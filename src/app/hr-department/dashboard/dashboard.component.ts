import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { Chart, registerables } from 'chart.js';
import { SubSink } from 'subsink';
Chart.register(...registerables);


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  all_departments: any[] = [];
  all_faculties: any[] = [];
  stats:any;
  facultylinegraph: any;
  departmentlinegraph: any;
  facultypiegraph: any;
  departmentpiegraph: any;
  private subs = new SubSink();

  constructor(private http: ConnectApiService, private ngzone: NgZone) { }

  showFLinechart(): void {
    let total: number[] = [];
    let facultyline = document.getElementById("fline") as HTMLCanvasElement;
    let facultypie = document.getElementById("f-pie") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(() => {
      this.facultylinegraph = new Chart(facultyline, {
        type: 'bar',
        data: {
          labels: this.all_faculties?.map((el: any) => el?.abb?.toUpperCase()),
          datasets: [{
            label: `Total employees`,
            data: this.all_faculties?.map((el: any) => el.number_of_employees),
            // fill: false,
            borderColor: '#002469',
            backgroundColor: "#002469"
            // tension: 0.1
          },
          {
            label: `Total supervisors`,
            data: this.all_faculties?.map((el: any) => el.supervisors),
            // fill: false,
            borderColor: '#D22730',
            backgroundColor: "#D22730"
            // tension: 0.1
          }
          ]
        },
        options: {
          // animation:{duration:400}
          scales: {
            y: {
              beginAtZero: true,
              grid: {
                display: false
              }
            },
            x: {
              grid:{
                display: false
              }
            }
          },
          plugins: {
            title: {
              display: true,
              text: 'Employees By Faculty'
            }
          }
        }
      });

      this.facultypiegraph = new Chart(facultypie, {
        type: 'doughnut',
        data: {
          labels: this.all_faculties?.map((el: any) => el?.abb?.toUpperCase()),
          datasets: [
            {
              label: 'Employees',
              data: this.all_faculties?.map((el: any) => el.number_of_employees),
              backgroundColor: [
                "rgb(27, 44, 93)",
                "rgb(252, 175, 23)",
                "rgb(218, 33, 40)"
              ],
            }
          ]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Employees by faculty'
            }
          }
        },
      })
    });
  }

  showDLinechart(): void {
    var total: number[] = [];
    var departline = document.getElementById("dline") as HTMLCanvasElement;
    var departpie = document.getElementById("d-pie") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(() => {
      this.departmentlinegraph = new Chart(departline, {
        type: 'bar',
        data: {
          labels: this.all_departments?.map((el: any) => el?.abb?.toUpperCase()),
          datasets: [{
            label: `Total employees`,
            data: this.all_departments?.map((el: any) => el.number_of_employees),
            // fill: false,
            borderColor: '#002469',
            backgroundColor: "#002469"
            // tension: 0.1
          },
          {
            label: `Total supervisors`,
            data: this.all_departments?.map((el: any) => el.supervisors),
            // fill: false,
            borderColor: '#D22730',
            backgroundColor: "#D22730"
            // tension: 0.1
          }
          ]
        },
        options: {
          // animation:{duration:400}
          scales: {
            y: {
              beginAtZero: true,
              grid: {
                display: false
              }
            },
            x:{
              grid: {
                display: false
              }
            }
          },
          plugins: {
            title: {
              display: true,
              text: 'Employees By Department'
            }
          }
        }
      });

      this.facultypiegraph = new Chart(departpie, {
        type: 'doughnut',
        data: {
          labels: this.all_departments?.map((el: any) => el?.abb?.toUpperCase()),
          datasets: [
            {
              label: 'Employees',
              data: this.all_departments?.map((el: any) => el.number_of_employees),
              backgroundColor: [
                "rgb(27, 44, 93)",
                "rgb(252, 175, 23)",
                "rgb(218, 33, 40)"
              ],
            }
          ]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Employees by faculty'
            }
          }
        },
      })
    });
  }

  ngOnInit(): void {
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems, {});

    this.subs.sink = this.http.get('/get-all-statistics').subscribe((data: any) => {
      // console.log(data);
      this.all_departments = data.department;
      this.all_faculties = data.faculty;
      this.stats = data.stats;
      this.showFLinechart();
      this.showDLinechart();
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
