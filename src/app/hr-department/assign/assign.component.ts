import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { Chart, registerables } from 'chart.js';
import { CalculateService } from 'src/app/services/calculate.service';
import { MatDialog } from '@angular/material/dialog';
import { ShowEmployeeRecordComponent } from '../show-employee-record/show-employee-record.component';
import { SubSink } from 'subsink';
Chart.register(...registerables);

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  employee_id: any;
  employee: any;
  soption: any = "";
  hoption: any = "";
  moption: any = "";
  doption: any = "";
  linegraph: any;
  piegraph: any;
  supervisors: any[] = [];
  managers: any[] = [];
  sp: any[] = [];
  mg: any[] = [];
  searchinput: string = "";
  searchmanagerinput: string = "";
  showAssign: boolean = false;
  showManager: boolean = false;
  totals: any;

  constructor(private actroute: ActivatedRoute, public http: ConnectApiService, public cal: CalculateService, private dialog: MatDialog, private ngzone: NgZone) { }
  showSupervisor(): void {
    this.showAssign = !this.showAssign;
  }

  showManager1(): void {
    this.showManager = !this.showManager;
  }

  changeEmployee(value: any): void {
    this.subs.sink = this.http.post('updateEmployee', {
      _id: this.employee_id,
      ...value
    }).subscribe((data: any) => {
      if (data.modifiedCount > 0) {
        this.linegraph.destroy();
        this.piegraph.destroy();
        this.getEmployee();
      }
      // console.log(data);
    })
  }

  showPiechart(): void {
    var pie = document.getElementById("pie") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(() => {
      this.piegraph = new Chart(pie, {
        type: 'doughnut',
        data: {
          labels: [
            'Behavioural Competencies',
            'Key Performance Areas (KPAs)'
          ],
          datasets: [{
            label: 'Employees Rating',
            data: [this.totals.bc, this.totals.kpa, (100 - (this.totals.bc + this.totals.kpa))],
            backgroundColor: [
              '#D22730',
              '#f0b323',
              '#002469',
              '#8b8075'
            ],
            hoverOffset: 4
          }]
        },
        options: {
          cutout: '80%',
          animation: { animateRotate: true },
          plugins: {
            title: {
              display: true,
              text: `${this.employee?.employee_record.slice(-1)[0]?.period ?? "Cycle Not Available"}`
            }
          }
        }
      });
    })
  }

  showLinechart(): void {
    var total: number[] = [];
    this.employee.employee_record.forEach((element: any) => {
      const { totalscore } = this.cal.calculateFinalScore(element);
      total.push(totalscore);
    });

    var line = document.getElementById("line") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(() => {
      this.linegraph = new Chart(line, {
        type: 'line',
        data: {
          labels: this.employee?.employee_record.map((el: any) => el.period?.slice(0, 8)),
          datasets: [{
            label: `Total Scores`,
            data: total,
            fill: false,
            borderColor: '#002469',
            tension: 0.1
          }]
        },
        options: {
          // animation:{duration:400}
          scales: {
            y: {
              beginAtZero: true
            }
          },
          plugins: {
            title: {
              display: true,
              text: `Employee's Total Scores For All Perfomance Cycles`
            }
          }
        }
      });
    });
  }

  getEmployee(): void {
    this.subs.sink = this.http.get(`get-employee-record/${this.employee_id}`)
      .subscribe((data: any) => {
        // console.log("Employee Record ",data);     
        this.employee = data;
        this.totals = this.cal.calculateFinalScore(data?.employee_record[data?.employee_record.length - 1]);

        this.http.get(`get-all-supervisor/${this.employee?.faculty}`)
          .subscribe((data: any) => {
            // console.log(data);
            this.sp = data?.filter((dt: any) => dt.email !== this.employee.email && dt.email !== this.employee.supervisor_name);
            this.supervisors = this.sp;
          });

        this.http.get(`get-all-manager/${this.employee?.faculty}`)
          .subscribe((data: any) => {
            // console.log(data);
            this.mg = data?.filter((dt: any) => dt.email !== this.employee.email && dt.email !== this.employee.manager_name);
            this.managers = this.mg;
          });
        this.showPiechart();
        this.showLinechart();
      });
  }

  search(): void {
    this.supervisors = this.sp.filter((data: any) => {
      if (data.email.toLowerCase().match(this.searchinput.toLowerCase()) || data.firstname.toLowerCase().match(this.searchinput.toLowerCase()) || data.lastname.toLowerCase().match(this.searchinput.toLowerCase())) {
        return data;
      }
    });
  }

  searchManager(): void {
    this.managers = this.mg.filter((data: any) => {
      if (data.email.toLowerCase().match(this.searchmanagerinput.toLowerCase()) || data.firstname.toLowerCase().match(this.searchmanagerinput.toLowerCase()) || data.lastname.toLowerCase().match(this.searchmanagerinput.toLowerCase())) {
        return data;
      }
    });
  }

  openAllRecordDialog(data: any): void {
    const allrcorddialog = this.dialog.open(ShowEmployeeRecordComponent, {
      width: '90%',
      hasBackdrop: true,
      data: { ...data, name: `${this.employee.firstname} ${this.employee.lastname}` }
    })
  }

  MtrackBy(index: number, manager: any) {
    return manager.email;
  }

  StrackBy(index: number, supervisor: any) {
    return supervisor.email;
  }

  PtrackBy(index: number, perf: any) {
    return perf.period;
  }

  ngOnInit(): void {
    this.employee_id = this.actroute.snapshot.params['id'];

    this.ngzone.runOutsideAngular(() => {
      let elems = document.querySelectorAll('.fixed-action-btn');
      let instances = M.FloatingActionButton.init(elems, {
        direction: 'top'
      });
    });
    this.getEmployee();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
