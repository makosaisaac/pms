import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEmployeeRecordComponent } from './show-employee-record.component';

describe('ShowEmployeeRecordComponent', () => {
  let component: ShowEmployeeRecordComponent;
  let fixture: ComponentFixture<ShowEmployeeRecordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowEmployeeRecordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEmployeeRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
