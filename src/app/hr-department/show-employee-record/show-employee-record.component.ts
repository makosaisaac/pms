import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CalculateService } from 'src/app/services/calculate.service';
// import jsPDF from 'jspdf';
// import html2canvas from 'html2canvas';

@Component({
  selector: 'app-show-employee-record',
  templateUrl: './show-employee-record.component.html',
  styleUrls: ['./show-employee-record.component.scss']
})
export class ShowEmployeeRecordComponent implements OnInit {
  bcdisplayedColumns: string[] = ['behaviour_competencies','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  kpadisplayedColumns: string[] = ['key_performance_areas','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  pdpdisplayedColumns: string[] = ['performance_gap', 'intervation_required', 'time_frame', 'Indicator_for_DGA', 'supervisor_comments'];
  pdpdataSource = [];
  kpadataSource = [];
  bcdataSource = [];
  totals: any;

  constructor(public dialogRef: MatDialogRef<ShowEmployeeRecordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public calc: CalculateService) { }

  // printPDF() {
  //   var content = document.getElementById('content') as HTMLDivElement;

  //   console.log("PDF", content.clientHeight);
  //   html2canvas(content).then(canvas => {

  //     let fileWidth = 208;
  //     let fileHeight = canvas.height * fileWidth / canvas.width;
  //     // let fileHeight = content.clientHeight;

  //     const FILEURI = canvas.toDataURL('image/png')
  //     let PDF = new jsPDF('p', 'mm', 'a4');
  //     let position = 0;
  //     PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight)

  //     PDF.save(`${this.data?.name}_${this.data?.period}.pdf`);
  //   });

  // }

  ngOnInit(): void {
    this.totals = this.calc.calculateFinalScore(this.data);
    this.pdpdataSource = this.data?.pdp;
    this.kpadataSource = this.data?.kpa;
    this.bcdataSource = this.data?.bc;
  }

}
