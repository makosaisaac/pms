import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AssignComponent } from './assign/assign.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HrDepartmentComponent } from './hr-department.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';

const routes: Routes = [
  {path:'',component:HrDepartmentComponent, children:[
    {path:'dashboard',component:DashboardComponent},
    {path:'view-employee/:id',component:AssignComponent},
    {path:'view-all-employees/:id',component:ViewEmployeeComponent},
    {path:'', redirectTo:'/hr-department/dashboard', pathMatch:'full'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HrDepartmentRoutingModule { }
