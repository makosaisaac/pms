import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

  errormsg:any;
  subs = new SubSink();

  constructor(private http:AuthService,private router:Router) { }

  login(form:NgForm){
    this.errormsg = "";
    if(form.valid){
      this.subs.sink = this.http.post('login',{
        email:form.value.email,
        password:form.value.password
      }).subscribe((data:any)=>{
        if(!!data?.message){
          this.errormsg = data?.message;
        }else{
          localStorage.setItem('qaZDpEWx4zgDLqL', data?.token);
          localStorage.setItem('id', data?._id);

          if(form.value.user==='employee' && !!data?.employee){
            this.router.navigate(["/employee"]);
          }else if(form.value.user==='supervisor' && !!data?.supervisor){
            this.router.navigate(["/supervisor"]);
          }else if(form.value.user==='manager' && !!data?.manager){
            this.router.navigate(["/manager"]);
          }else if(form.value.user==='dvc_vc' && !!data?.dvc_vc){
            this.router.navigate(["/dvc-vc"]);
          }else if(form.value.user==='hr_officer' && !!data?.hr_officer){
            this.router.navigate(["/hr-department"]);
          }else{
            this.errormsg = `access denied choose correct user type`
          }  
        }
        
        // if(data?.employee === true && data?.supervisor === false && data?.hr_officer === false){
        //   console.log(data);
        //   this.router.navigate(['/employee/dashboard']);
        // }else if(data?.employee === true && data?.supervisor === true && data?.hr_officer === false){
        //   this.router.navigate(['/auth/login-as',{sup:data._id}]);
        // }else if(data?.employee === true && data?.hr_officer === true && data?.supervisor === false){
        //   // this.router.navigate(['/hr-department/dashboard']);
        //   this.router.navigate(['/auth/login-as',{hr:data._id}]);
        // }else if(data?.employee === true && data?.hr_officer === true && data?.supervisor === true){
        //   console.log('Full Employee, Supervisor, Hr officer',data);
        //   this.router.navigate(['/auth/login-as',{sup:data._id,hr:data._id}]);
        // }else{
        //   this.errormsg = data?.message;
        // }
      })
    }
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

}
