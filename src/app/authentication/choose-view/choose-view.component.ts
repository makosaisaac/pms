import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-choose-view',
  templateUrl: './choose-view.component.html',
  styleUrls: ['./choose-view.component.scss']
})
export class ChooseViewComponent implements OnInit {

  sup:any;
  hr:any;
  constructor(private actroute:ActivatedRoute) { }

  ngOnInit(): void {
    this.sup = this.actroute.snapshot.params.sup;
    this.hr = this.actroute.snapshot.params.hr;

    console.log(this.sup);
    console.log(this.hr);

  }

}
