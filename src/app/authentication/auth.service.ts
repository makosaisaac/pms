import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

export interface User{
  _id:string;
  email:string;
  password:string;
  firstname:string;
  lastname:string;
  faculty:string;
  employee:boolean;
  supervisor:boolean;
  hr_officer:boolean;
  manager:boolean;
  dvc_vc:boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url:string = environment.production?window.location.origin+'/authentication/':"http://localhost:9090/authentication/";

  constructor(private http:HttpClient) { }

  post(link:any, data:any){
    return this.http.post(`${this.url}${link}`,data);
  }

  getUser(token:string): User{
    return JSON.parse(atob(token.split('.')[1])) as User;
  }
}
