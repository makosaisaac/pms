import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication.component';
import { ChooseViewComponent } from './choose-view/choose-view.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [{path:'',component:AuthenticationComponent, children:[
  {path:'login', component: LoginComponent},
  {path:'register', component: RegisterComponent},
  {path:'login-as', component: ChooseViewComponent},
  {path:'', redirectTo:'/auth/login',pathMatch:'full'}
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
