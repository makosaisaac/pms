import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy, AfterViewInit {
  jobLevels:number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
  probations:string[] = ["1st Probation", "2nd Probation", "3rd Probation", "4th Probation", "Probation Extension", "Not Applicable"]
  constructor(public http: AuthService, public data: ConnectApiService, private route: Router) { 
    this.jobLevels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
  }
  subs = new SubSink();

  registerUser(form: NgForm) {    
    const user = {
      email: form.value.em,
      password: form.value.pass,
      firstname: form.value.fn,
      lastname: form.value.ln,
      faculty: form.value.fac,
      job_grade: form.value.jb,
      position: form.value.po,
      probation: form.value.pr,
      nature_of_appointment: form.value.na,
      date_of_appointment: form.value.da,
      // performance_cycle:form.value.pc,
      period_under_review: form.value.pu,
    };

    // console.log(user);
    this.subs.sink = this.http.post('register', user)
      .subscribe((data: any) => {
        // console.log(data);
        if (data?.email) {
          localStorage.setItem('id', data._id);
          localStorage.setItem('qaZDpEWx4zgDLqL', data.token);
          this.route.navigate(['/employee/dashboard']);
        }
      })

  }

  ngOnInit(): void {
    // var elems = document.querySelectorAll('.datepicker');
    // var instances = M.Datepicker.init(elems, {});

    // if (this.data.faculty.length > 0) {
    //   var elems1 = document.querySelectorAll('select');
    //   var instances1 = M.FormSelect.init(elems1, {});
    // }
  }

  ngAfterViewInit(): void {
    var elems1 = document.querySelectorAll('select');
    var instances1 = M.FormSelect.init(elems1, {});
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
