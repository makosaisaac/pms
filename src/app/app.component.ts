import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router, RouterEvent } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PerfomanceManagementSystem';

  constructor(updates: SwUpdate, router: Router, private loading: LoadingService) {
    router.events.subscribe((event: RouterEvent | any): void => {
      if (event instanceof NavigationStart) {
        this.loading.requestStarted();
      } else if (event instanceof NavigationEnd) {
        this.loading.requestEnded();
      }
    });

    updates.versionUpdates.subscribe(event => {
      console.log('Version is', event);
      updates.activateUpdate().then(()=> document.location.reload());
    });
  }

}
