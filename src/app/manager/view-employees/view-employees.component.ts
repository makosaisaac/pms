import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { Chart, registerables } from 'chart.js';
import { SubSink } from 'subsink';
Chart.register(...registerables);

@Component({
  selector: 'app-view-employees',
  templateUrl: './view-employees.component.html',
  styleUrls: ['./view-employees.component.scss']
})
export class ViewEmployeesComponent implements OnInit,OnDestroy {
  supervisorID:any;
  employeeData:any = null;
  employees:any[] = [];
  pending:any[] = [];
  completed:any[] = [];
  nocycle:any[] = [];
  piegraph:any;
  name:string = "";
  subs = new SubSink();

  constructor(private http:ConnectApiService, private actroute:ActivatedRoute,private ngzone:NgZone) { }

  showPiechart():void{
    let pie = document.getElementById("pie") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(()=>{
      this.piegraph = new Chart(pie,{
        type: 'pie',
        data: {
          labels: [
            'No Perfomance Cycle',
            'Completed',
            'Pending'
          ],
          datasets: [{
            label: 'Employees Rating',
            data: [this.nocycle.length, this.completed.length, this.pending.length],
            backgroundColor: [
              '#D22730',
              '#f0b323',
              '#002469',
              '#8b8075'
            ],
            hoverOffset: 4
          }]
        },
        options:{
          // cutout: '80%',
          animation:{animateRotate:true},
          plugins:{
            title: {
              display: true,
              text: `All Employee Statistics`
            }
          }
        }
      });
    });
  }

  getEmployeeData(data:any ,name:string){
    if(data?.employees_record?.length>0){
      this.name = name;
      this.employeeData = data?.employees_record?.slice(-1)[0];
    }else{
      this.employeeData = null;
      this.piegraph?.destroy();
      this.showPiechart();
    }
  }

  complete(complete:string):void{
    if(complete === 'updated'){
      this.employeeData = null;
      this.pending = [];
      this.completed = [];
      this.nocycle = [];
      this.getData();
    }
  }

  getData(){
    this.ngzone.runOutsideAngular(()=>{
      var elems = document.querySelectorAll('.collapsible');
      var instances = M.Collapsible.init(elems, {});
    });

    this.subs.sink = this.http.get(`get-employees-record-data/${this.supervisorID}`).subscribe((data:any)=>{
      this.employees = data;

      for (let i = 0; i < data.length; i++) {
        if(data[i].employees_record.length>0){
          var record = data[i].employees_record.slice(-1)[0];  
          if(!!record?.pre?.manager_comments){
            this.completed.push(data[i]);
          }else{
            this.pending.push(data[i]);
          }
        }else{
          this.nocycle.push(data[i]);
        }
      }
      this.piegraph?.destroy();
      this.showPiechart();
    });
  }

  CtrackBy(index:number,item:any){
    return item._id;
  }

  PtrackBy(index:number,item:any){
    return item._id;
  }

  NtrackBy(index:number,item:any){
    return item._id;
  }

  ngOnInit(): void {
    this.supervisorID = this.actroute.snapshot.params.id;
    this.piegraph?.destroy();
    this.getData();
  }

  ngOnDestroy(): void {
      this.piegraph.destroy();
      this.subs.unsubscribe();
  }

}
