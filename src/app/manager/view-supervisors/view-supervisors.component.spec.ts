import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSupervisorsComponent } from './view-supervisors.component';

describe('ViewSupervisorsComponent', () => {
  let component: ViewSupervisorsComponent;
  let fixture: ComponentFixture<ViewSupervisorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewSupervisorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSupervisorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
