import { Component, OnDestroy, OnInit } from '@angular/core';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-view-supervisors',
  templateUrl: './view-supervisors.component.html',
  styleUrls: ['./view-supervisors.component.scss']
})
export class ViewSupervisorsComponent implements OnInit, OnDestroy {
  m_id: any = localStorage.getItem("id");
  searchText: string = "";
  su: any[] = [];
  supervisors: any[] = [];
  subs = new SubSink();
  constructor(private http: ConnectApiService) { }

  searchSu(): void {
    if (this.searchText === '') {
      this.supervisors = this.su;
    } else {
      this.supervisors = this.su.filter((el) => {
        if (el?.firstname?.toLowerCase().match(this.searchText.toLowerCase()) || el?.lastname?.toLowerCase().match(this.searchText.toLowerCase()) || el?.email?.toLowerCase().match(this.searchText.toLowerCase())) {
          return el;
        }
      })
    }
  }

  trackBy(index: number, item: any) {
    return item.email
  }

  ngOnInit(): void {
    this.subs.sink = this.http.get(`get-manager-supervisors/${this.m_id}`).subscribe((data: any) => {
      this.su = data;
      this.supervisors = data;
    })
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
