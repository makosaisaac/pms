import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from '../employee/employee.component';
import { ManagerComponent } from './manager.component';
import { ViewEmployeesComponent } from './view-employees/view-employees.component';
import { ViewSupervisorsComponent } from './view-supervisors/view-supervisors.component';

const routes: Routes = [
  {
    path: "", component: ManagerComponent,
    children: [
      { path: "view-supervisors", component: ViewSupervisorsComponent },
      { path: "view-employees/:id", component: ViewEmployeesComponent },
      // {path:"employee/:id",component:EmployeeComponent},
      { path: "", redirectTo: "/manager/view-supervisors", pathMatch: "full" }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule { }
