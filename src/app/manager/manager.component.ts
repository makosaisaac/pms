import { Component, NgZone, OnInit } from '@angular/core';
import { AuthService } from '../authentication/auth.service';
import { ConnectApiService } from '../services/connect-api.service';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit {
  user:any;
  accounts = ["Supervisor", "Employee"];
  constructor(private http:ConnectApiService,private auth:AuthService,private ngzone:NgZone) { }

  switchaccount(){
    var cou = document.querySelector('.carousel');
    cou?.classList.toggle('active');
  }

  ngOnInit(): void {
    var id: any = this.auth.getUser(<string>localStorage.getItem("qaZDpEWx4zgDLqL"))?._id;
    this.http.get(`get-user-data/${id}`).subscribe((data:any)=>{
      this.user = data;

      if(this.user.supervisor===false){
        var p = document.getElementById("el1");
        p?.remove();
      }
      
      if(this.user.dvc_vc===false){
        var p = document.getElementById("el2");
        p?.remove();
      }

      if(this.user.hr_officer===false){
        var q = document.getElementById("el3");
        q?.remove();
      }
      
      if(this.user.manager===true){  
        this.ngzone.runOutsideAngular(()=>{
          var elems = document.querySelectorAll('.carousel');
          var instances = M.Carousel.init(elems, {});
        });
      }

    })

  }

}
