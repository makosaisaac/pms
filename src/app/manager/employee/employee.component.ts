import { Component, Input,Output, EventEmitter, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CalculateService } from 'src/app/services/calculate.service';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit,OnChanges,OnDestroy {
  @Input() data:any;
  @Input() name:any;
  @Output() complete = new EventEmitter<string>();
  pdpdisplayedColumns: string[] = ['performance_gap', 'intervation_required', 'time_frame', 'Indicator_for_DGA','supervisor_comments'];
  bcdisplayedColumns: string[] = ['behaviour_competencies','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  kpadisplayedColumns: string[] = ['key_performance_areas','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  totals:any = 0;
  subs = new SubSink();
  constructor(public calc:CalculateService, private http:ConnectApiService) { }

  comments(form:NgForm){
    const dt = {
      email:this.data?.email,
      period:this.data?.period,
      "pre.manager_comments":form.value.des
    }

    // console.log("Data  ",dt);
   
    this.subs.sink = this.http.post('update-employee-record',dt).subscribe((data:any)=>{
      // console.log(data);
        this.http.openSnackBar(data.message);  
       this.complete.emit(data?.message); 
    });
  }

  ngOnInit(): void {
    this.totals = this.calc.calculateFinalScore(this.data);
  }

  ngOnChanges(changes: SimpleChanges){
    this.totals = this.calc.calculateFinalScore(this.data);
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

}
