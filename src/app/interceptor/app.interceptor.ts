import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { LoadingService } from '../services/loading.service';
import { Router } from '@angular/router';


@Injectable()
export class AppInterceptor implements HttpInterceptor {

  constructor(private loadService: LoadingService,private router:Router) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const token = localStorage.getItem("qaZDpEWx4zgDLqL");
    
    this.loadService.requestStarted();

    if (token) {
      const cloned_request = request.clone({
        headers: request.headers.set("Authorization", "Bearer " + token)
      });
      return this.handler(next, cloned_request);
    } else {
      return this.handler(next, request);
    }

  }

  handler(next: HttpHandler, request: HttpRequest<any>) {
    return next.handle(request).pipe(
      tap((event: any) => {
        if (event instanceof HttpResponse) {
          this.loadService.requestEnded();
        }
      }
        , (error: HttpErrorResponse) => {
          console.log(error);
          this.loadService.resetRequest();
        }
      )
    )
  }
}
