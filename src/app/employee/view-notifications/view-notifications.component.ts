import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-view-notifications',
  templateUrl: './view-notifications.component.html',
  styleUrls: ['./view-notifications.component.scss']
})
export class ViewNotificationsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ViewNotificationsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http: ConnectApiService) { }

  ngOnInit(): void {
  }

}

