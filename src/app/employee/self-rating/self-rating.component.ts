import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-self-rating',
  templateUrl: './self-rating.component.html',
  styleUrls: ['./self-rating.component.scss']
})
export class SelfRatingComponent implements OnInit {
  rating:string = "";

  constructor(public dialogRef: MatDialogRef<SelfRatingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http:ConnectApiService) { }

  submitBC(){
    const data = {
      email:this.data.email,
      period:this.data.period
    }

    console.log("Hello ",this.data._id);
    

    if(!!this.data.behaviour_competencies){
      this.http.post('self-grading',{...data,rating:this.rating,bc:this.data._id}).subscribe((data:any)=>{
        if(data.modifiedCount>0){
          this.dialogRef.close({updated:true})
        }
      });
    }else if(!!this.data.key_performance_areas){
      this.http.post('self-grading',{...data,rating:this.rating,kpa:this.data._id}).subscribe((data:any)=>{
        if(data.modifiedCount>0){
          this.dialogRef.close({updated:true})
        }
      });
    }
  }

  ngOnInit(): void {
    this.rating = this.data.self_rating;
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
  }

}
