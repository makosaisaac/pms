import { Component, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CalculateService } from 'src/app/services/calculate.service';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';
import { ApprovalComponent } from '../approval/approval.component';

@Component({
  selector: 'app-pdp',
  templateUrl: './pdp.component.html',
  styleUrls: ['./pdp.component.scss']
})
export class PdpComponent implements OnInit,OnDestroy {
  displayedColumns: string[] = ['performance_gap', 'intervation_required', 'time_frame', 'Indicator_for_DGA','supervisor_comments'];
  dataSource = [];
  em_id:any = localStorage.getItem('id');
  pdpFormGroup: UntypedFormGroup | any;
  hide:boolean = false;
  private subs = new SubSink();


  constructor(private http:ConnectApiService,public calc:CalculateService,private _formBuilder: UntypedFormBuilder,private dialog:MatDialog) { }

  currentPeriod:any;
  getData(){
   this.subs.sink = this.http.get(`get-employee-record/${this.em_id}`).subscribe((data:any)=>{
      this.currentPeriod = data?.employee_record.slice(-1)[0];
      this.dataSource = this.currentPeriod?.pdp;
    })
  }

  create:boolean = false;
  activate(){
    // this.create = !this.create;
    const dialog = this.dialog.open(ApprovalComponent,{
      width:'55%',
      data:{email:this.currentPeriod.email,period:this.currentPeriod.period}
    });

    this.subs.sink = dialog.afterClosed().subscribe((dt:any)=>{
      if(dt?.updated){
        this.getData();
      }
    });
  }

  submitPDP(){
    // console.log(this.pdpFormGroup.value);
    if(this.pdpFormGroup.invalid) return
    var pdp = this.pdpFormGroup.value;
    // console.log(this.currentPeriod);
    if(!!this.currentPeriod){
      this.subs.sink = this.http.post('add-employee-record',{
        email:this.currentPeriod.email,
        period:this.currentPeriod.period,
        pdp
      }).subscribe((data:any)=>{
          if(data)
            this.pdpFormGroup?.reset();
          this.getData();
      });
    }
  }

  hidePDP(){
    this.hide = !this.hide;
  }

  trackBy(index:number,item:any){
    return item.performance_gap
  }

  ngOnInit(): void {
    this.getData();
    this.pdpFormGroup = this._formBuilder.group({
      performance_gap:['',Validators.required],
      intervation_required:['',Validators.required],
      time_frame:['',Validators.required],
      Indicator_for_DGA:['',Validators.required]
    });
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

}
