import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatRippleModule} from '@angular/material/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import {MatBadgeModule} from '@angular/material/badge';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin
import {NgxPrintModule} from 'ngx-print';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeComponent } from './employee.component';
import { BcComponent } from './bc/bc.component';
import { KpaComponent } from './kpa/kpa.component';
import { PdpComponent } from './pdp/pdp.component';
import { HomeComponent } from './home/home.component';
import { SelfRatingComponent } from './self-rating/self-rating.component';
import { ViewFullRecordComponent } from './view-full-record/view-full-record.component';
import { ApprovalComponent } from './approval/approval.component';
import { ViewNotificationsComponent } from './view-notifications/view-notifications.component';


FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [
    EmployeeComponent,
    BcComponent,
    KpaComponent,
    PdpComponent,
    HomeComponent,
    SelfRatingComponent,
    ViewFullRecordComponent,
    ApprovalComponent,
    ViewNotificationsComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MatTableModule,
    MatIconModule,
    MatRippleModule,
    MatTooltipModule,
    MatMenuModule,
    MatDialogModule,
    MatBadgeModule,
    FullCalendarModule,
    ReactiveFormsModule,FormsModule,
    NgxPrintModule
  ]
})
export class EmployeeModule { }
