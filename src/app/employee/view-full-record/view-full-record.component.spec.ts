import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFullRecordComponent } from './view-full-record.component';

describe('ViewFullRecordComponent', () => {
  let component: ViewFullRecordComponent;
  let fixture: ComponentFixture<ViewFullRecordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewFullRecordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFullRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
