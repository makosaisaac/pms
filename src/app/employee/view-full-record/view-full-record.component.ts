import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CalculateService } from 'src/app/services/calculate.service';

@Component({
  selector: 'app-view-full-record',
  templateUrl: './view-full-record.component.html',
  styleUrls: ['./view-full-record.component.scss']
})
export class ViewFullRecordComponent implements OnInit {
  bcdisplayedColumns: string[] = ['behaviour_competencies','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  kpadisplayedColumns: string[] = ['key_performance_areas','key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  pdpdisplayedColumns: string[] = ['performance_gap', 'intervation_required', 'time_frame', 'Indicator_for_DGA','supervisor_comments'];
  pdpdataSource = [];
  kpadataSource = [];
  bcdataSource = [];
  totals:any;

  constructor(public dialogRef: MatDialogRef<ViewFullRecordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public calc:CalculateService) { }

  ngOnInit(): void {
    // console.log(this.data);
    this.totals = this.calc.calculateFinalScore(this.data);
    this.pdpdataSource = this.data?.pdp;
    this.kpadataSource = this.data?.kpa;
    this.bcdataSource = this.data?.bc;

  }

}
