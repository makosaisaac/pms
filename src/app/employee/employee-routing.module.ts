import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BcComponent } from './bc/bc.component';
import { EmployeeComponent } from './employee.component';
import { HomeComponent } from './home/home.component';
import { KpaComponent } from './kpa/kpa.component';
import { PdpComponent } from './pdp/pdp.component';

const routes: Routes = [
  { path: '', component: EmployeeComponent, children: [
    {path:'bc',component:BcComponent},
    {path:'kpa',component:KpaComponent},
    {path:'pdp',component:PdpComponent},
    {path:'dashboard',component:HomeComponent},
    {path:'', redirectTo:'/employee/dashboard', pathMatch:'full'}
  ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}
