import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CalculateService } from 'src/app/services/calculate.service';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { SubSink } from 'subsink';
import { ApprovalComponent } from '../approval/approval.component';
import { SelfRatingComponent } from '../self-rating/self-rating.component';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-bc',
  templateUrl: './bc.component.html',
  styleUrls: ['./bc.component.scss']
})
export class BcComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  displayedColumns: string[] = ['behaviour_competencies', 'key_indicators', 'self_rating', 'supervisor_rating','comments','weight','final_score'];
  dataSource = [];
  em_id:any = localStorage.getItem('id');

  constructor(private http:ConnectApiService,public calc:CalculateService,private dialog:MatDialog) { }

  currentPeriod:any;
  getData(){
    this.subs.sink = this.http.get(`get-employee-record/${this.em_id}`).subscribe((data:any)=>{
      this.currentPeriod = data?.employee_record.slice(-1)[0];
      this.dataSource = this.currentPeriod?.bc;
    })
  }

  create:boolean = false;
  activate(){
    // this.create = !this.create;
    const dialog = this.dialog.open(ApprovalComponent,{
      width:'50%',
      minWidth:'300px',
      data:{email:this.currentPeriod.email,period:this.currentPeriod.period}
    });

    this.subs.sink = dialog.afterClosed().subscribe((dt:any)=>{
      // console.log(dt);
      if(dt?.updated){
        this.getData();
      }
    });
  }

  trackBy(index:number,item:any){
    return item.behaviour_competencies;
  }

  openRatingDialog(data:any){

    if (!this.currentPeriod?.ppc?.employee_approval){
      const dialog = this.dialog.open(SelfRatingComponent,{
        width:'30%',
        minWidth:'300px',
        data:{...data,email:this.currentPeriod.email,period:this.currentPeriod.period}
      });

     this.subs.sink =  dialog.afterClosed().subscribe((dt:any)=>{
        // console.log(dt);
        if(dt?.updated){
          this.getData();
        }
      });
    }

  }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

}
