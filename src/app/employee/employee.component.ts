import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ConnectApiService } from '../services/connect-api.service';
import * as moment from "moment";
import { SubSink } from 'subsink';
import { AuthService } from '../authentication/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewNotificationsComponent } from './view-notifications/view-notifications.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit, OnDestroy {
  user: any;
  meetings: any[] = [];
  count: number = 0;
  private subs = new SubSink();

  constructor(private http: ConnectApiService,private auth:AuthService, private ngzone: NgZone,private dialog:MatDialog) { }

  switchaccount() {
    var cou = document.querySelector('.carousel');
    cou?.classList.toggle('active');
  }

  time(time: Date) {
    return moment(time).format('LLLL');
  }

  viewNotification(meeting: Object | any) {
  this.subs.sink = this.http.post('update-notis', {
      email: this.user.email,
      title: meeting?.title
    }).subscribe((data: any) => {
      // console.log(data);
      if (data.modifiedCount > 0) {
        this.subs.sink = this.http.get(`get-meetings/${this.user.email}`).subscribe((data: any) => {
          this.meetings = data;
          for (let i = 0; i < this.meetings.length; i++) {
            if (!this.meetings[i].viewed) {
              this.count++;
            }
          }
        });
      }
    });

    const notifDialog = this.dialog.open(ViewNotificationsComponent,{
      width:'30%',
      minWidth: '350px',
      data:{
        ...meeting
      }
    })
  }

  ngOnInit(): void {
    var id: any = this.auth.getUser(<string>localStorage.getItem("qaZDpEWx4zgDLqL"))?._id;
    this.subs.sink = this.http.get(`get-user-data/${id}`).subscribe((data: any) => {
      this.user = data;
      // console.log("User ",this.user);
      if (this.user.manager === false) {
        var p = document.getElementById("el1");
        p?.remove();
      }

      if (this.user.dvc_vc === false) {
        var p = document.getElementById("el2");
        p?.remove();
      }

      if (this.user.hr_officer === false) {
        var q = document.getElementById("el4");
        q?.remove();
      }

      if (this.user.supervisor === false) {
        var q = document.getElementById("el3");
        q?.remove();
      }

      this.ngzone.runOutsideAngular(() => {
        var elems = document.querySelectorAll('.carousel');
        var instances = M.Carousel.init(elems, {});
      });

      this.subs.sink = this.http.get(`get-meetings/${this.user.email}`).subscribe((data: any) => {
        this.meetings = data;

        for (let i = 0; i < this.meetings.length; i++) {
          if (!this.meetings[i].viewed) {
            this.count++;
          }
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
