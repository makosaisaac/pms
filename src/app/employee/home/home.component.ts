import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { CalculateService } from 'src/app/services/calculate.service';
import { ConnectApiService } from 'src/app/services/connect-api.service';
import { Chart, registerables } from 'chart.js';
import { MatDialog } from '@angular/material/dialog';
import { ViewFullRecordComponent } from '../view-full-record/view-full-record.component';
import { CalendarOptions } from '@fullcalendar/angular';
import { SubSink } from 'subsink';
Chart.register(...registerables);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  em_id: any = localStorage.getItem('id');
  user: any;
  employee: any;
  linegraph: any;
  piegraph: any;
  totals: any;
  meetings: any[] = [];
  private subs = new SubSink();
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    events: [],
  };

  changeView(event: any): any {
    // this.calendarOptions.initialView = "timeGridDay";
    alert(event)
  }

  constructor(private http: ConnectApiService, public cal: CalculateService, private dialog: MatDialog, private ngzone: NgZone) { }
  showPiechart(): void {
    var pie = document.getElementById("pie") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(() => {
      this.piegraph = new Chart(pie, {
        type: 'doughnut',
        data: {
          labels: [
            'Behavioural Competencies',
            'Key Performance Areas (KPAs)'
          ],
          datasets: [{
            label: 'Employees Rating',
            data: [this.totals.bc, this.totals.kpa, (100 - (this.totals.bc + this.totals.kpa))],
            backgroundColor: [
              '#D22730',
              '#f0b323',
              '#002469',
              '#8b8075'
            ],
            hoverOffset: 4
          }]
        },
        options: {
          cutout: '80%',
          animation: { animateRotate: true },
          responsive: true,
          plugins: {
            title: {
              display: true,
              text: `${this.employee?.employee_record.slice(-1)[0]?.period ?? "Cycle Not Available"}`
            }
          }
        }
      });
    })
  }

  showLinechart(): void {
    var total: number[] = [];
    this.employee?.employee_record.forEach((element: any) => {
      const { totalscore } = this.cal.calculateFinalScore(element);
      total.push(totalscore);
    });

    var line = document.getElementById("line") as HTMLCanvasElement;
    this.ngzone.runOutsideAngular(() => {
      this.linegraph = new Chart(line, {
        type: 'line',
        data: {
          labels: this.employee?.employee_record.map((el: any) => el.period?.slice(0, 9)),
          datasets: [{
            label: `Total Scores`,
            data: total,
            fill: false,
            borderColor: '#002469',
            tension: 0.1
          }]
        },
        options: {
          // animation:{duration:400}
          scales: {
            x: {
              beginAtZero: true
            },
            y: {
              beginAtZero: true
            }
          },
          plugins: {
            title: {
              display: true,
              text: `Employee's Total Scores For All Perfomance Cycles`
            }
          }
        }
      });
    });
  }

  getEmployee() {
    this.subs.sink = this.http.get(`get-user-data/${this.em_id}`).subscribe((data: any) => {
      this.user = data;
    });
    // console.log(this.em_id);
    this.subs.sink = this.http.get(`get-employee-record/${this.em_id}`)
      .subscribe((data: any) => {
        // console.log("Employee Record ",data);
        this.employee = data;
        // console.log(data?.employee_record.slice(-1)[0]);
        this.totals = this.cal.calculateFinalScore(data?.employee_record.slice(-1)[0]);
        this.showPiechart();
        this.showLinechart();
        this.subs.sink = this.http.get(`get-meetings/${this.employee.email}`).subscribe((data: any) => {
          this.meetings = data.map((mp: any) => ({ title: mp.title, date: mp.date_time }));
          this.calendarOptions.events = this.meetings;
          // this.calendarOptions.eventClick = this.changeView(event)
        });
      });
  }

  trackBy(index: number, employee: Object | any) {
    return employee.period;
  }

  openAllRecordDialog(data: any) {
    const allrcorddialog = this.dialog.open(ViewFullRecordComponent, {
      width: '90%',
      hasBackdrop: true,
      data: { ...data, name: `${this.user.firstname} ${this.user.lastname}` }
    })
  }

  ngOnInit(): void {
    this.getEmployee();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
