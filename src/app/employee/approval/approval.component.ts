import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectApiService } from 'src/app/services/connect-api.service';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.scss']
})
export class ApprovalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ApprovalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private http:ConnectApiService, private formBuilder:UntypedFormBuilder) { }

  ppc:UntypedFormGroup | any;
  pre:UntypedFormGroup | any;

  comment(){
    var com = {
      ...this.data,
      "ppc.employee_comments":this.ppc.value.employee_comments,
      "ppc.employee_approval":this.ppc.value.employee_approval,
      "pre.employee_comments":this.pre.value.employee_comments
    }
    
    this.http.post('update-employee-record',com)
    .subscribe((data:any)=>{
      // console.log(data);
      if(data.message === 'updated')
        this.dialogRef.close({updated:`updated`});
    });
  }

  ngOnInit(): void {
    this.ppc = this.formBuilder.group({
      employee_comments:["", Validators.required],
      employee_approval:["", Validators.required]
    });

    this.pre = this.formBuilder.group({
      employee_comments:["", Validators.required]
    });

    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
  }


}
