import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructionComponent } from './instruction.component';
import { InstructionRoutingModule } from './instruction-routing.module';



@NgModule({
  declarations: [
    InstructionComponent
  ],
  imports: [
    CommonModule,
    InstructionRoutingModule
  ]
})
export class InstructionModule { }
